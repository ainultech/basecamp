import Covid19Section from "../components/structures/Covid19Section";
import { NextSeo } from "next-seo";
import TwitterSEO from "../components/TwitterSEO";

export async function getStaticProps() {
  const apiUrlCovid19Global = process.env.COVID_19_GLOBAL_API_URL;
  const apiOptionCovid19Global = { method: "GET" };
  const apiDataCovid19Global = await fetch(
    apiUrlCovid19Global,
    apiOptionCovid19Global
  );
  const dataFromCovid19GlobalAPI = await apiDataCovid19Global.json();

  const apiUrlCovid19Indonesia = process.env.COVID_19_INDONESIA_API_URL;
  const apiOptionCovid19Indonesia = { method: "GET" };
  const apiDataCovid19Indonesia = await fetch(
    apiUrlCovid19Indonesia,
    apiOptionCovid19Indonesia
  );
  const dataFromCovid19IndonesiaAPI = await apiDataCovid19Indonesia.json();

  const apiUrlCovid19VaccineIndonesia =
    process.env.COVID_19_VACCINE_INDONESIA_API_URL;
  const request = require("request");
  const csv = require("csvtojson");
  const apiDataCovid19VaccineIndonesia = await csv().fromStream(
    request.get(apiUrlCovid19VaccineIndonesia)
  );

  return {
    props: {
      resultsCovid19Global: dataFromCovid19GlobalAPI,
      resultsCovid19Indonesia: dataFromCovid19IndonesiaAPI,
      resultsCovid19VaccineIndonesia: apiDataCovid19VaccineIndonesia,
    },
    revalidate: 60,
  };
}

export default function Covid19({
  resultsCovid19Global,
  resultsCovid19Indonesia,
  resultsCovid19VaccineIndonesia,
}) {
  const seotitle = `Covid19 Tracking - Ainul.tech`;
  const seodescrip = `A simple and cute designed Covid19 tracking page. Ainul wrote it for his mother and friends.`;
  const seourl = `https://ahmad.ainul.tech/covid19`;
  const seopreviewimg = "/static/covid19-trancking-feature-img.png";

  return (
    <div>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="w-full flex items-center justify-center mt-6 md:mt-[100px] px-4 md:px-0 py-4 md:py-0">
        <Covid19Section
          dataGlobal={resultsCovid19Global}
          dataIndonesia={resultsCovid19Indonesia}
          dataVaccineIndonesia={resultsCovid19VaccineIndonesia}
        />
      </div>
      <div className="flex items-center justify-center mt-16 md:mt-28">
        <div className="mt-4 flex items-center space-x-2">
          <div>
            <img
              src="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/SWWcYfE3GXeCGo1xPi/giphy.gif"
              className="w-20 h-auto"
            />
          </div>
          <div className="pl-2">
            <img
              src="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/1Fr6GHv8WB4KRBvbTb/giphy.gif"
              className="w-16 h-auto"
            />
          </div>
          <div>
            <img
              src="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/ED1YHJEnOESA49KT9P/giphy.gif"
              className="w-28 h-auto"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
