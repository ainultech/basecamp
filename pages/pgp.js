import { Copy } from "akar-icons";
import PGPHeader from "../components/structures/pgp/PGPHeader";
import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import PGPFooter from "../components/structures/pgp/PGPFooter";

export default function Pgp() {
  const [isAllCopied1, setIsAllCopied1] = useState(false);
  const [isEmailCopied1, setIsEmailCopied1] = useState(false);

  const onCopyAllText1 = () => {
    setIsAllCopied1(true);
    setTimeout(() => {
      setIsAllCopied1(false);
    }, 3000);
  };

  const onCopyEmailText1 = () => {
    setIsEmailCopied1(true);
    setTimeout(() => {
      setIsEmailCopied1(false);
    }, 3000);
  };

  const gpgKey = [
    "-----BEGIN PGP PUBLIC KEY BLOCK-----",
    "",
    "mQINBGDYklABEADeHzEjwgkdPZrkLZWdO6/EMVX2JK2cyQumIYGlcJqk6vYLkSHr",
    "civv50txc4eViK7JEPpAArv+ncRaFMaSNGWUM7/5JetKF9So14FJdVHtd2Ge+guu",
    "659OKBaw60fpckttZnLU1ePU8G1hm5w2kd0Q5mr0pIZOyPM1+9Fp1F2W418zA+WL",
    "pTGbKEk/94/x9mCNIU6KWKPB/wTiaCSw3q0Dn6eIV8Edtt+XM0B2+aYpcniElJ+u",
    "fKstw/mj8tRv15gaLAfqtaHvyKfzi4duqpzbXxo5HtJvESZaHAWvzmrUsoT7I1k7",
    "3PA4SRK5GAZNRh8UbYZiWEU42u5r+dkwb8cAufh657djkkQOw981QTRdHAX3AcQi",
    "VvcvpQWc0oB/iJc7VVb0TnPvQUJQDMWO9lkGybze6uqTfP6i4VJFKaCOGpu7G4ZA",
    "TBuoqy7gptBsKPVvDJdDAKWRv35EDRw59jOFAULTR7HbBN+nsKFsxl/Ex7PruKXI",
    "99ZHkb5/yXnqzCCZ4fAi0oxIz9r7Odw/MZv6Vbpnc7jAOf29+SRXxAALnN06/24P",
    "b9Vv/dn2+2PzhJwStzIsaqTIw2OKnNECTCoYE/cJF/oUrZyHpwF+70l+aRcrcKKQ",
    "vvImrhj9wIulUEw+pzAjC8cpXJ5R90vlWeKosnWTMzIwK7bfJpjP4f8U/wARAQAB",
    "tCBBaG1hZCBBaW51bC5SIDxhaG1hZEBhaW51bC50ZWNoPokDeQQTAQgBYwIbAwUJ",
    "HhM4AAIZAQQLBwkDBRUICgIDBBYAAQIWIQSdhc7lSXvX8Fc75G5Hv3GdB1/YzQUC",
    "YO2HxlMUgAAAAAASADhwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3R3aXR0ZXIu",
    "Y29tL2FpbnVsdGVjaC9zdGF0dXMvMTQxNDkyNDgyNjk1NTgyOTI0OEQUgAAAAAAS",
    "AClwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL2dpdGxhYi5jb20vYWludWx0ZWNo",
    "L2dpdGxhYl9wcm9vZjIUgAAAAAASABdwcm9vZkBtZXRhY29kZS5iaXpkbnM6YWlu",
    "dWwudGVjaD90eXBlPVRYVF0UgAAAAAASAEJwcm9vZkBtZXRhY29kZS5iaXpodHRw",
    "czovL2dpc3QuZ2l0aHViLmNvbS9haW51bHRlY2gvOTIzYzQzMTQzYjlkNGZiNDdl",
    "OGJlMTA5OTliMWMxODAACgkQR79xnQdf2M3RjRAA1f/O3jgAugiYYgLbO7kGpdlM",
    "ouEegSdd9Q9Ruj0JHNH4MlaAyEIdouF5lleH2stft4NrzoVdzU9p9nhj/oeOtFFZ",
    "tp497Mp/hHADAL7aduukXs4N+qrBbvChYHnpdlv6MX83PoM/on72RPNjFRLXm9XG",
    "fNkhxdICC7ZEIlVWcGHpJxmykkIoxSxuLhy48NHKKkLSiYont1UUX575GG1fq8ks",
    "52fZG4EiVaFUu08jrfeoG+weuFhkOIVO/vmtyz09sTyyK0GlgLMsPXXhznyifLbX",
    "NttJBLMquqK3ipTHg5CqWSm3xdu2eT5vRAVd4m5/ObGJ0wBc38rhgSxB8NaHN58t",
    "nGzNv1ZUIxuq8R0sdZdxv5++bilDF0xVokZIKTd4+N3OuK3x8631z0dJAwhgJr1m",
    "onjyVZ/3jPdqn27ccdGrY6JAZbbC3jOIxq7XoMbjJ5ya0LXEpMkmmQAeT8cDaX4O",
    "8po5R9w6d0LyNUdM7Oyg5xSMKywZgZ1e6YX22b3yAaG0ZRNmMXcVY+RMbXqi9Kjk",
    "gUIHV3r1VbmZRQ4U7FQo3CUHWtSbtMQVSefb4D9VyDviJliM8KyXu9ysbT0Rrt+N",
    "dGrt6w8IxMFJ7Vc1p9Pu9o6Iaov/LY3L07xXy30bSKjlVgMAkhCkfALOKLhddRMZ",
    "j7qFIUNtzHxGFhkuDNW0KUFobWFkIEFpbnVsLlIgPGFobWFkQGluZGllYWxpc3Rp",
    "Yy5zdHVkaW8+iQKIBBMBCAByAhsDBQkeEzgABAsHCQMFFQgKAgMEFgABAhYhBJ2F",
    "zuVJe9fwVzvkbke/cZ0HX9jNBQJg7YWJOxSAAAAAABIAIHByb29mQG1ldGFjb2Rl",
    "LmJpemRuczppbmRpZWFsaXN0aWMuc3R1ZGlvP3R5cGU9VFhUAAoJEEe/cZ0HX9jN",
    "5wQP/ixDSDmZ72adgcpS9lLREsFLU0n+k4EgeRVkRnNdHitZdw7nDDM9zBWilKlP",
    "3vBVltNB0KaEFKJOSweBt9/bys/9Bw2xKG4yVzFVeCO8orfEo8wiXcPeSWmBBmh2",
    "pjnRvEkMQhVXckXSgNnDiv7GNSQb/5q6y8mbkkqkgUeBlo39v4LD8ZH8QmMyq1C4",
    "wiZgI+zsWkK1ljHarKF/1+GxP7S5Ve6MzxIjKaw+0uD7mtEMao1RaP0dTQrrUZxN",
    "eK91pnnwMsnUQnEhRI4s7lptCM1dO02cHpZ46tPS1X0FnicxMH2ypKzZMGVsnM/5",
    "AWe2b6HWkuI1r12m4kKYMouxfwEcI+saFiABJcbBZD1dIQoVBTG3P2Gs4kTijLv+",
    "uwmdaaSSo2PRLGAof25pI7zwVR/SwSkC26DtlEiwFDfCS1UWSL9djLLEW8LNJ8TK",
    "FAnpog668cYTb7ksQVXBN21Y33/SGiJb/5ue9JmJkbRvNcK2EAbYtdcm5fERiENf",
    "f+BPILObmspuB2v9gRqoFfgnireXrW8XiX6JUzspQw7tth0a/izzskX+8FegaPlV",
    "v45Q82i4euaNyNf8cKr5sK3LoWIdwz43odrnL1OnflBHgCaKZCOMXwo0XWM6amEj",
    "rR3iIUE7+bzZzpfIMqKzCIvF+FlIHmk6otigPVWfv+/19uDatC1JbmRpZWFsaXN0",
    "aWMgU3R1ZGlvIDxoZXlAaW5kaWVhbGlzdGljLnN0dWRpbz6JAlQEEwEKAD4WIQSd",
    "hc7lSXvX8Fc75G5Hv3GdB1/YzQUCYP4rmQIbAwUJHhM4AAULCQgHAgYVCgkICwIE",
    "FgIDAQIeAQIXgAAKCRBHv3GdB1/YzdVMD/4kviYvyyxNQCs/ilsY3LrvL1n1+Y4S",
    "6WkI2GCzNwewmIQQ1cjELBjGfLc1drwkgLuQPsGY5Qhvz7ubt/WMELiqN/DRH6ii",
    "OTrPTaJBaeKDjLN9c0xF+TG+xEJOsmPkVdITdVSF4J+9J/Xpa1wgno51aB4u//io",
    "5ThVTgF+eAIAzSoAG9lREV6Fd5ABmzoJ/kcYGv5tRxoM8P271Huro/6DbAATsWac",
    "ga1CkTCM0cZWFxRrmpsjs1kuK+wNPBZVO39rmySzGRuRwUNRfAYV+uvGsUjARy9Z",
    "ZmGVZCLyFd7EmE/YwCDCdnheo2pckKcUdFupeB1OgWtSD0uoDj8juQiJLygjlWyb",
    "G8noBqEsHwKcJDzvfAvQ3g9upjLVLuY8rZ93SVXWVGAT3O9wwQhY6VwSlAtY4hLt",
    "sKQnMyIkyACHEbIvh6GW/2jLnnbnIuQ5sqTA+kFZDgswognMSKuQEF1N1MN7ricY",
    "2uCXDEAKTiXb20U9z3cy+HJKKgG/hnmhiF9GVQUKK0NLbkOeOinYXe+kTJBD93dZ",
    "jrR5DhrSs6OOgbf1Hbh3TGtR77kuvQq+Q9x9ZPdi6/ZLR3bJX+oUATWDR/dilcvl",
    "MHUy3/d9aZGyfq4KwaROtvTGTgbKtSaOOvSQGbhcEYRHChzCSdSnCYd2O/Iv+3DD",
    "r08LBe1PMB/KTrkCDQRg2JJQARAA5Jg8/iTOg4cHjrgdxpJq+6l1yf9rSrQCcdh+",
    "3Lifu99scU/byk+k4KmMta0Y+Re+KUglg29CVm6qoZua74k4jSuhDxy6H7RVXzcA",
    "2OJaZpyfG8x0C2QIERoCk3GIK5glIL8T/wX3HoHpQju+CyTbpo1BLYu9SHqsof0p",
    "4H65grzP3mQmYEuQ7+3ELVIn5xHDnRwFDFZrlcm+VIPMB2p1qRhp20sjnd46qDep",
    "dIGKXuxmLKHlyEnLAdWrMTRiG3vDL0+Ep/ywTlfDYLVsd6x2vywCzagvaUxC4kUX",
    "tX1gMW4qbF82JfzRYzveyWFTFGNZ4sIVq92QsqHUSHlmQA6zE/bYZvhNXB0fd+bP",
    "gpOkz57ArbhsATGHWR940fRwWcnUH7PIVcd+Xyd6SG7n0bA9yKempyKVPg+OTXRq",
    "1k3MrS3ZTT/9d1SeVpx1ry8IwZTDUOehGOUlHrl9wQXqodxjg5YME9XwK9PJtG0D",
    "59Hu1OnkyrWe/DVM9sqkhjwFFBtFFmjL/vctvkHTvRLiLW+qYU5v0Op7FxOodbgs",
    "LbDTb0W1kuojdTYyXpqwY1SwbEwtUotS+SRCm52Ubxn8dyjVYhO2FdcDoFwE0nWB",
    "b+XcNa3dx5kkc5PYDaNc7DHKLmt6FTLoexZgo2xwWGGPf9MF2RXf7QlSsItLhwnn",
    "8opAik8AEQEAAYkCNQQYAQgAKQUCYNiSUAkQR79xnQdf2M0CGwwFCR4TOAAECwcJ",
    "AwUVCAoCAwQWAAECAACznxAAikxPO45hz7qU8v0apb6zePMr01xR+Y6TT6X0ALyd",
    "zRERFVSvdx/SUWrimYSS7vHRPovUwHRfwRfqbKHL/VEDfb8gQo9MV+Fo7smrBlpQ",
    "zF1EUQi+0dfDIt0ClIEmWm1kkZ2PyMaatWe1MEArR7iRX+0ZGgt60BtBxt/BFmjd",
    "6/nl3bGZWE5tWEfjn1KVoDzo+WDeCXwnhrEX3xT+mKn3pGxny5ZPe2oLvZ60g6IB",
    "jiI3Kd/0on/6JVkKJRH2YJXbOirnODXZZYEI+koKp33bmAIS0x3B4AHE0g3lPxca",
    "BaWQblOys2rC9ZTCv0EsRUgl6MaDttfF/3r1z/7SjmCNQrtAdqfxrlX4E+RmfVUx",
    "SJyHzDnlcPOwcSAKbSAwmzFHS5lUjiqJVeciTpEX5txJmKqIlYD1v34mV3m6KweF",
    "Iovk0Ke6br5SSTwihB/v0of6LbabAdH+YJd0Y43EFGnrMB00+vzh/tq4nbiJ/N7y",
    "ffSy6cw2Lkv2nhe1RekYBMfxkNXQ7dyhewM1Dzq0FmiI/+2a6ahrjanXcMuk1Izw",
    "Q+u2jNQdZPs7mVnTksMhpst3tVDIYERePeE0lRoU72y10Fl3jdm2VTbbOB8F9WEl",
    "8fJ6zyk6x5o29buCWAtd/qXPt0bjvFp7Tmcss7e+w6ZTY6AfiaDjV0uMiJ7Wmpek",
    "fug=",
    "=IyyB",
    "-----END PGP PUBLIC KEY BLOCK-----",
  ].join("\n");

  const myEmail = [
    "ahmad@ainul.tech",
    "ahmad@indiealistic.studio",
    "hey@indiealistic.studio",
  ].join("\n");

  return (
    <div className="p-4">
      <PGPHeader />
      <div className="items-start mx-auto w-full max-w-2xl mt-7 space-y-6">
        <div className="flex space-x-4">
          <div className="w-[75%] bg-gray-100 dark:bg-gray-800 p-4 rounded-lg">
            <div className="block md:flex space-x-0 md:space-x-2 items-center">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Name
              </h3>
              <p className="text-black dark:text-white uppercase text-xs">
                Ahmad Ainul Rizki
              </p>
            </div>
            <p className="text-black dark:text-white font-sourcecodepro uppercase text-base">
              9D85 CEE5 497B D7F0 573B E46E 47BF 719D 075F D8CD
            </p>
            <p className="text-black dark:text-white text-sm uppercase pt-2">
              Email List:{" "}
              <ul>
                <li>
                  - <strong>ahmad@ainul.tech</strong>
                </li>
                <li>
                  - <strong>ahmad@indiealistic.studio</strong>
                </li>
                <li>
                  - <strong>hey@indiealistic.studio</strong>
                </li>
              </ul>
            </p>
          </div>
          <div className="w-[25%] grid grid-cols-1 gap-4">
            <CopyToClipboard text={gpgKey} onCopy={onCopyAllText1}>
              <div className="p-2 bg-gray-100 dark:bg-gray-800 block md:flex items-center space-y-2 md:space-y-0 md:space-x-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                <Copy
                  className={`text-black dark:text-white mx-auto md:mx-0 ${
                    isAllCopied1 ? "text-purple-400" : "text-black"
                  }`}
                  size={22}
                />
                <p
                  className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                    isAllCopied1 ? "text-purple-400" : "text-black"
                  }`}
                >
                  {isAllCopied1 ? "Copied" : "Copy Key"}
                </p>
              </div>
            </CopyToClipboard>
            <CopyToClipboard text={myEmail} onCopy={onCopyEmailText1}>
              <div className="p-2 bg-gray-100 dark:bg-gray-800 block md:flex items-center space-y-2 md:space-y-0 md:space-x-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                <Copy
                  className={`text-black dark:text-white mx-auto md:mx-0 ${
                    isEmailCopied1 ? "text-purple-400" : "text-black"
                  }`}
                  size={22}
                />
                <p
                  className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                    isEmailCopied1 ? "text-purple-400" : "text-black"
                  }`}
                >
                  {isEmailCopied1 ? "Copied" : "Copy Email"}
                </p>
              </div>
            </CopyToClipboard>
          </div>
        </div>

        <div className="flex space-x-4">
          <div className="w-[100%] bg-gray-100 dark:bg-gray-800 p-4 rounded-lg">
            <p className="overflow-auto text-black dark:text-white font-sourcecodepro text-base">
              -----BEGIN PGP PUBLIC KEY BLOCK-----
              <br />
              <br />
              mQINBGDYklABEADeHzEjwgkdPZrkLZWdO6/EMVX2JK2cyQumIYGlcJqk6vYLkSHr
              <br />
              civv50txc4eViK7JEPpAArv+ncRaFMaSNGWUM7/5JetKF9So14FJdVHtd2Ge+guu
              <br />
              659OKBaw60fpckttZnLU1ePU8G1hm5w2kd0Q5mr0pIZOyPM1+9Fp1F2W418zA+WL
              <br />
              pTGbKEk/94/x9mCNIU6KWKPB/wTiaCSw3q0Dn6eIV8Edtt+XM0B2+aYpcniElJ+u
              <br />
              fKstw/mj8tRv15gaLAfqtaHvyKfzi4duqpzbXxo5HtJvESZaHAWvzmrUsoT7I1k7
              <br />
              3PA4SRK5GAZNRh8UbYZiWEU42u5r+dkwb8cAufh657djkkQOw981QTRdHAX3AcQi
              <br />
              VvcvpQWc0oB/iJc7VVb0TnPvQUJQDMWO9lkGybze6uqTfP6i4VJFKaCOGpu7G4ZA
              <br />
              TBuoqy7gptBsKPVvDJdDAKWRv35EDRw59jOFAULTR7HbBN+nsKFsxl/Ex7PruKXI
              <br />
              99ZHkb5/yXnqzCCZ4fAi0oxIz9r7Odw/MZv6Vbpnc7jAOf29+SRXxAALnN06/24P
              <br />
              b9Vv/dn2+2PzhJwStzIsaqTIw2OKnNECTCoYE/cJF/oUrZyHpwF+70l+aRcrcKKQ
              <br />
              vvImrhj9wIulUEw+pzAjC8cpXJ5R90vlWeKosnWTMzIwK7bfJpjP4f8U/wARAQAB
              <br />
              tCBBaG1hZCBBaW51bC5SIDxhaG1hZEBhaW51bC50ZWNoPokDeQQTAQgBYwIbAwUJ
              <br />
              HhM4AAIZAQQLBwkDBRUICgIDBBYAAQIWIQSdhc7lSXvX8Fc75G5Hv3GdB1/YzQUC
              <br />
              YO2HxlMUgAAAAAASADhwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3R3aXR0ZXIu
              <br />
              Y29tL2FpbnVsdGVjaC9zdGF0dXMvMTQxNDkyNDgyNjk1NTgyOTI0OEQUgAAAAAAS
              <br />
              AClwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL2dpdGxhYi5jb20vYWludWx0ZWNo
              <br />
              L2dpdGxhYl9wcm9vZjIUgAAAAAASABdwcm9vZkBtZXRhY29kZS5iaXpkbnM6YWlu
              <br />
              dWwudGVjaD90eXBlPVRYVF0UgAAAAAASAEJwcm9vZkBtZXRhY29kZS5iaXpodHRw
              <br />
              czovL2dpc3QuZ2l0aHViLmNvbS9haW51bHRlY2gvOTIzYzQzMTQzYjlkNGZiNDdl
              <br />
              OGJlMTA5OTliMWMxODAACgkQR79xnQdf2M3RjRAA1f/O3jgAugiYYgLbO7kGpdlM
              <br />
              ouEegSdd9Q9Ruj0JHNH4MlaAyEIdouF5lleH2stft4NrzoVdzU9p9nhj/oeOtFFZ
              <br />
              tp497Mp/hHADAL7aduukXs4N+qrBbvChYHnpdlv6MX83PoM/on72RPNjFRLXm9XG
              <br />
              fNkhxdICC7ZEIlVWcGHpJxmykkIoxSxuLhy48NHKKkLSiYont1UUX575GG1fq8ks
              <br />
              52fZG4EiVaFUu08jrfeoG+weuFhkOIVO/vmtyz09sTyyK0GlgLMsPXXhznyifLbX
              <br />
              NttJBLMquqK3ipTHg5CqWSm3xdu2eT5vRAVd4m5/ObGJ0wBc38rhgSxB8NaHN58t
              <br />
              nGzNv1ZUIxuq8R0sdZdxv5++bilDF0xVokZIKTd4+N3OuK3x8631z0dJAwhgJr1m
              <br />
              onjyVZ/3jPdqn27ccdGrY6JAZbbC3jOIxq7XoMbjJ5ya0LXEpMkmmQAeT8cDaX4O
              <br />
              8po5R9w6d0LyNUdM7Oyg5xSMKywZgZ1e6YX22b3yAaG0ZRNmMXcVY+RMbXqi9Kjk
              <br />
              gUIHV3r1VbmZRQ4U7FQo3CUHWtSbtMQVSefb4D9VyDviJliM8KyXu9ysbT0Rrt+N
              <br />
              dGrt6w8IxMFJ7Vc1p9Pu9o6Iaov/LY3L07xXy30bSKjlVgMAkhCkfALOKLhddRMZ
              <br />
              j7qFIUNtzHxGFhkuDNW0KUFobWFkIEFpbnVsLlIgPGFobWFkQGluZGllYWxpc3Rp
              <br />
              Yy5zdHVkaW8+iQKIBBMBCAByAhsDBQkeEzgABAsHCQMFFQgKAgMEFgABAhYhBJ2F
              <br />
              zuVJe9fwVzvkbke/cZ0HX9jNBQJg7YWJOxSAAAAAABIAIHByb29mQG1ldGFjb2Rl
              <br />
              LmJpemRuczppbmRpZWFsaXN0aWMuc3R1ZGlvP3R5cGU9VFhUAAoJEEe/cZ0HX9jN
              <br />
              5wQP/ixDSDmZ72adgcpS9lLREsFLU0n+k4EgeRVkRnNdHitZdw7nDDM9zBWilKlP
              <br />
              3vBVltNB0KaEFKJOSweBt9/bys/9Bw2xKG4yVzFVeCO8orfEo8wiXcPeSWmBBmh2
              <br />
              pjnRvEkMQhVXckXSgNnDiv7GNSQb/5q6y8mbkkqkgUeBlo39v4LD8ZH8QmMyq1C4
              <br />
              wiZgI+zsWkK1ljHarKF/1+GxP7S5Ve6MzxIjKaw+0uD7mtEMao1RaP0dTQrrUZxN
              <br />
              eK91pnnwMsnUQnEhRI4s7lptCM1dO02cHpZ46tPS1X0FnicxMH2ypKzZMGVsnM/5
              <br />
              AWe2b6HWkuI1r12m4kKYMouxfwEcI+saFiABJcbBZD1dIQoVBTG3P2Gs4kTijLv+
              <br />
              uwmdaaSSo2PRLGAof25pI7zwVR/SwSkC26DtlEiwFDfCS1UWSL9djLLEW8LNJ8TK
              <br />
              FAnpog668cYTb7ksQVXBN21Y33/SGiJb/5ue9JmJkbRvNcK2EAbYtdcm5fERiENf
              <br />
              f+BPILObmspuB2v9gRqoFfgnireXrW8XiX6JUzspQw7tth0a/izzskX+8FegaPlV
              <br />
              v45Q82i4euaNyNf8cKr5sK3LoWIdwz43odrnL1OnflBHgCaKZCOMXwo0XWM6amEj
              <br />
              rR3iIUE7+bzZzpfIMqKzCIvF+FlIHmk6otigPVWfv+/19uDatC1JbmRpZWFsaXN0
              <br />
              aWMgU3R1ZGlvIDxoZXlAaW5kaWVhbGlzdGljLnN0dWRpbz6JAlQEEwEKAD4WIQSd
              <br />
              hc7lSXvX8Fc75G5Hv3GdB1/YzQUCYP4rmQIbAwUJHhM4AAULCQgHAgYVCgkICwIE
              <br />
              FgIDAQIeAQIXgAAKCRBHv3GdB1/YzdVMD/4kviYvyyxNQCs/ilsY3LrvL1n1+Y4S
              <br />
              6WkI2GCzNwewmIQQ1cjELBjGfLc1drwkgLuQPsGY5Qhvz7ubt/WMELiqN/DRH6ii
              <br />
              OTrPTaJBaeKDjLN9c0xF+TG+xEJOsmPkVdITdVSF4J+9J/Xpa1wgno51aB4u//io
              <br />
              5ThVTgF+eAIAzSoAG9lREV6Fd5ABmzoJ/kcYGv5tRxoM8P271Huro/6DbAATsWac
              <br />
              ga1CkTCM0cZWFxRrmpsjs1kuK+wNPBZVO39rmySzGRuRwUNRfAYV+uvGsUjARy9Z
              <br />
              ZmGVZCLyFd7EmE/YwCDCdnheo2pckKcUdFupeB1OgWtSD0uoDj8juQiJLygjlWyb
              <br />
              G8noBqEsHwKcJDzvfAvQ3g9upjLVLuY8rZ93SVXWVGAT3O9wwQhY6VwSlAtY4hLt
              <br />
              sKQnMyIkyACHEbIvh6GW/2jLnnbnIuQ5sqTA+kFZDgswognMSKuQEF1N1MN7ricY
              <br />
              2uCXDEAKTiXb20U9z3cy+HJKKgG/hnmhiF9GVQUKK0NLbkOeOinYXe+kTJBD93dZ
              <br />
              jrR5DhrSs6OOgbf1Hbh3TGtR77kuvQq+Q9x9ZPdi6/ZLR3bJX+oUATWDR/dilcvl
              <br />
              MHUy3/d9aZGyfq4KwaROtvTGTgbKtSaOOvSQGbhcEYRHChzCSdSnCYd2O/Iv+3DD
              <br />
              r08LBe1PMB/KTrkCDQRg2JJQARAA5Jg8/iTOg4cHjrgdxpJq+6l1yf9rSrQCcdh+
              <br />
              3Lifu99scU/byk+k4KmMta0Y+Re+KUglg29CVm6qoZua74k4jSuhDxy6H7RVXzcA
              <br />
              2OJaZpyfG8x0C2QIERoCk3GIK5glIL8T/wX3HoHpQju+CyTbpo1BLYu9SHqsof0p
              <br />
              4H65grzP3mQmYEuQ7+3ELVIn5xHDnRwFDFZrlcm+VIPMB2p1qRhp20sjnd46qDep
              <br />
              dIGKXuxmLKHlyEnLAdWrMTRiG3vDL0+Ep/ywTlfDYLVsd6x2vywCzagvaUxC4kUX
              <br />
              tX1gMW4qbF82JfzRYzveyWFTFGNZ4sIVq92QsqHUSHlmQA6zE/bYZvhNXB0fd+bP
              <br />
              gpOkz57ArbhsATGHWR940fRwWcnUH7PIVcd+Xyd6SG7n0bA9yKempyKVPg+OTXRq
              <br />
              1k3MrS3ZTT/9d1SeVpx1ry8IwZTDUOehGOUlHrl9wQXqodxjg5YME9XwK9PJtG0D
              <br />
              59Hu1OnkyrWe/DVM9sqkhjwFFBtFFmjL/vctvkHTvRLiLW+qYU5v0Op7FxOodbgs
              <br />
              LbDTb0W1kuojdTYyXpqwY1SwbEwtUotS+SRCm52Ubxn8dyjVYhO2FdcDoFwE0nWB
              <br />
              b+XcNa3dx5kkc5PYDaNc7DHKLmt6FTLoexZgo2xwWGGPf9MF2RXf7QlSsItLhwnn
              <br />
              8opAik8AEQEAAYkCNQQYAQgAKQUCYNiSUAkQR79xnQdf2M0CGwwFCR4TOAAECwcJ
              <br />
              AwUVCAoCAwQWAAECAACznxAAikxPO45hz7qU8v0apb6zePMr01xR+Y6TT6X0ALyd
              <br />
              zRERFVSvdx/SUWrimYSS7vHRPovUwHRfwRfqbKHL/VEDfb8gQo9MV+Fo7smrBlpQ
              <br />
              zF1EUQi+0dfDIt0ClIEmWm1kkZ2PyMaatWe1MEArR7iRX+0ZGgt60BtBxt/BFmjd
              <br />
              6/nl3bGZWE5tWEfjn1KVoDzo+WDeCXwnhrEX3xT+mKn3pGxny5ZPe2oLvZ60g6IB
              <br />
              jiI3Kd/0on/6JVkKJRH2YJXbOirnODXZZYEI+koKp33bmAIS0x3B4AHE0g3lPxca
              <br />
              BaWQblOys2rC9ZTCv0EsRUgl6MaDttfF/3r1z/7SjmCNQrtAdqfxrlX4E+RmfVUx
              <br />
              SJyHzDnlcPOwcSAKbSAwmzFHS5lUjiqJVeciTpEX5txJmKqIlYD1v34mV3m6KweF
              <br />
              Iovk0Ke6br5SSTwihB/v0of6LbabAdH+YJd0Y43EFGnrMB00+vzh/tq4nbiJ/N7y
              <br />
              ffSy6cw2Lkv2nhe1RekYBMfxkNXQ7dyhewM1Dzq0FmiI/+2a6ahrjanXcMuk1Izw
              <br />
              Q+u2jNQdZPs7mVnTksMhpst3tVDIYERePeE0lRoU72y10Fl3jdm2VTbbOB8F9WEl
              <br />
              8fJ6zyk6x5o29buCWAtd/qXPt0bjvFp7Tmcss7e+w6ZTY6AfiaDjV0uMiJ7Wmpek
              <br />
              fug=
              <br />
              =IyyB
              <br />
              -----END PGP PUBLIC KEY BLOCK-----
            </p>
          </div>
        </div>
      </div>
      <PGPFooter />
    </div>
  );
}
