import { Envelope, MoreHorizontalFill, LockOff, Send } from "akar-icons";
import { NextSeo, SocialProfileJsonLd } from "next-seo";
import NextLink from "next/link";

export default function Ainul() {
  const seotitle = `Ahmad Ainul.R - Ainul.tech`;
  const seodescrip = `Ainul's personal page on Ainul's website (laughs). This page is mainly created to aggregate my information only, in addition, it is also used to check SEO`;
  const seourl = `https://ahmad.ainul.tech/human`;
  const seopreviewimg = "/static/ainul-tech-feature-img.png";

  return (
    <div className="dark:bg-black bg-blawhiteck">
      <main className="flex flex-col justify-center space-y-4 py-7 px-4 bg-purple-100 m-4">
        <NextSeo
          title={seotitle}
          description={seodescrip}
          canonical={seourl}
          openGraph={{
            seotitle,
            seodescrip,
            seourl,
            images: [
              {
                url: seopreviewimg,
                alt: seotitle,
                width: 1200,
                height: 628,
              },
            ],
          }}
        />
        <SocialProfileJsonLd
          type="Person"
          name="Ahmad Ainul Rizki"
          url="https://ahmad.ainul.tech"
          sameAs={[
            "https://bitclout.com/u/ainul",
            "https://www.instagram.com/ainultech/",
            "https://www.linkedin.com/in/ainultech/",
            "https://twitter.com/ainultech",
          ]}
        />
        <div className="items-start mx-auto w-full max-w-xl space-y-4">
          <div className="relative w-24 h-24 mx-auto rounded-full">
            <img
              className="object-cover absolute rounded-full transform origin-center scale-150"
              src="/ainul-memoji-nonbg.png"
            />
          </div>
          <div className="space-y-1">
            <div className="flex justify-center items-center">
              <h1 className="text-black font-bold text-xl inline-flex">
                Ahmad Ainul.R
              </h1>
            </div>
            <p className="text-black text-center text-base uppercase font-sourcecodepro">
              @ainul
            </p>
          </div>
          <div className="grid grid-cols-1 gap-2">
            <a
              href="mailto:ahmad@ainul.tech?subject=An%20email%20from%20AINUL.TECH&body=Hello%20Ainul%2C%0D%0A%0D%0AMy%20name%20is%20_________%0D%0A%0D%0AI%20got%20a%20something%20to%20tell%20you%3A"
              target="_blank"
              rel="noreferrer"
            >
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                <Envelope className="text-[#7C56FB]" />
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Email: ahmad@ainul.tech
                </h3>
              </div>
            </a>
            <NextLink href="/pgp">
              <a>
                <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                  <LockOff className="text-[#7C56FB]" />
                  <h3 className="font-sourcecodepro text-sm uppercase">
                    PGP Key-ID: 47BF719D075FD8CD
                  </h3>
                </div>
              </a>
            </NextLink>
            <a href="https://t.me/ainultech" target="_blank" rel="noreferrer">
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                <Send className="text-[#7C56FB]" />
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Telegram: @ainultech
                </h3>
              </div>
            </a>
          </div>
          <div className="flex items-center space-x-1">
            <div className="w-7 h-7 relative">
              <img
                className="object-cover absolute"
                src="/emoji/emoji-look.png"
              />
            </div>
            <h2 className="text-black font-sourcecodepro uppercase text-base">
              My networks
            </h2>
          </div>
          <div className="grid grid-cols-2 gap-2">
            <a
              href="https://twitter.com/ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Twitter
                </h3>
              </div>
            </a>
            <a
              href="https://bitclout.com/u/ainul"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  BitClout
                </h3>
              </div>
            </a>
            <a
              href="https://www.instagram.com/ainultech/"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Instagram
                </h3>
              </div>
            </a>
            <a
              href="https://vero.co/ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Vero
                </h3>
              </div>
            </a>
            <a
              href="https://github.com/ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Github
                </h3>
              </div>
            </a>
            <a
              href="https://gitlab.com/ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Gitlab
                </h3>
              </div>
            </a>
            <a
              href="https://keybase.io/ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Keybase
                </h3>
              </div>
            </a>
            <a
              href="https://satellite.earth/@ainul"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Satellite
                </h3>
              </div>
            </a>
            <a
              href="https://www.producthunt.com/@ainultech"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  ProductHunt
                </h3>
              </div>
            </a>
            <a
              href="https://join.status.im/u/0x049459587138cf0d7061f4dde7e9abc4324b463e5c33c7317284902855ac8fdcb85bdfbf88b2e06083186e7b5389116fabb03317c3c1b7da041cf1570cde6109e3"
              target="_blank"
              rel="noreferrer"
            >
              <div className=" border-2 border-[#7C56FB] text-[#7C56FB] p-3 hover:bg-purple-200 group">
                <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12 origin-center inline-flex">
                  Status
                </h3>
              </div>
            </a>
          </div>
          <div className="flex items-center space-x-1">
            <div className="w-7 h-7 relative">
              <img
                className="object-cover absolute"
                src="/emoji/emoji-diamond.png"
              />
            </div>
            <h2 className="text-black font-sourcecodepro uppercase text-base">
              My personal sites
            </h2>
          </div>
          <div className="grid grid-cols-1 gap-2">
            <NextLink href="/">
              <a>
                <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200 group">
                  <h3 className="font-sourcecodepro text-sm uppercase transform transition-all group-hover:scale-110 group-hover:-rotate-12">
                    AINUL.TECH
                  </h3>
                </div>
              </a>
            </NextLink>
          </div>
          <div className="flex items-center space-x-1">
            <div className="w-7 h-7 relative">
              <img
                className="object-cover absolute"
                src="/emoji/emoji-brain.png"
              />
            </div>
            <h2 className="text-black font-sourcecodepro uppercase text-base">
              My project sites
            </h2>
          </div>
          <div className="grid grid-cols-1 gap-2">
            <a
              href="https://indiealistic.studio"
              target="_blank"
              rel="noreferrer"
            >
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Indiealistic Studio
                </h3>
              </div>
            </a>
            <a
              href="https://another-worlds.ainul.tech"
              target="_blank"
              rel="noreferrer"
            >
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Another Worlds
                </h3>
              </div>
            </a>
            <a
              href="https://almost-vegan.ainul.tech"
              target="_blank"
              rel="noreferrer"
            >
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200">
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Almost Vegan
                </h3>
              </div>
            </a>
            <a
              href="https://xn--2i8hd227bgla.y.at"
              target="_blank"
              rel="noreferrer"
            >
              <div className="border-2 border-[#7C56FB] text-[#7C56FB] p-3 flex items-center space-x-2 hover:bg-purple-200 group">
                <div className="flex items-center">
                  <div className="w-7 h-7 relative transform transition-all group-hover:scale-150 group-hover:-rotate-12 group-hover:delay-75">
                    <img
                      className="object-cover absolute"
                      src="/emoji/emoji-bread.png"
                    />
                  </div>
                  <div className="w-7 h-7 relative transform transition-all group-hover:scale-150 group-hover:-rotate-12 group-hover:delay-100">
                    <img
                      className="object-cover absolute"
                      src="/emoji/emoji-cheese.png"
                    />
                  </div>
                  <div className="w-7 h-7 relative transform transition-all group-hover:scale-150 group-hover:-rotate-12 group-hover:delay-150">
                    <img
                      className="object-cover absolute"
                      src="/emoji/emoji-pancakes.png"
                    />
                  </div>
                  <div className="w-7 h-7 relative transform transition-all group-hover:scale-150 group-hover:-rotate-12 group-hover:delay-200">
                    <img
                      className="object-cover absolute"
                      src="/emoji/emoji-steaming-bowl.png"
                    />
                  </div>
                </div>
                <h3 className="font-sourcecodepro text-sm uppercase">
                  Y.AT | Another Worlds
                </h3>
              </div>
            </a>
          </div>
          <div className="items-start mx-auto w-full max-w-3xl space-y-1 flex justify-center">
            <MoreHorizontalFill className="text-black" />
          </div>
          <div className="flex justify-between">
            <p className="text-black font-sourcecodepro uppercase text-base text-center">
              Love{" "}
              <a
                className="text-[#7C56FB]"
                href="https://www.google.com/search?q=purple&oq=purple&aqs=chrome..69i57j46i433j0l7j46.790j1j4&sourceid=chrome&ie=UTF-8"
                target="_blank"
                rel="noreferrer"
              >
                purple
              </a>{" "}
              and Crypto-Anarchism
            </p>
            <p className="text-black font-sourcecodepro uppercase text-base text-center">
              17+2
            </p>
          </div>
        </div>
      </main>
    </div>
  );
}
