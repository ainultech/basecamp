import Container from "../../components/Container";
import NextLink from "next/link";
import LetterByAinul from "../../components/elements/LetterByAinul";
import {
  Designer,
  Developer,
  Developing,
  Configuration,
} from "../../components/elements/StatusBadge";
import {
  ArrowUpRight,
  Envelope,
  Globe,
  Heart,
  Inbox,
  MoreHorizontalFill,
  Pencil,
  Plant,
  PointingUp,
  RockOn,
  VictoryHand,
} from "akar-icons";
import useTranslate from "next-translate/useTranslation";
import { NextSeo } from "next-seo";
import TwitterSEO from "../../components/TwitterSEO";
import INDCoin from "../../components/elements/INDCoin";

export default function INDIEALISTIC() {
  const { t } = useTranslate("projects");

  const seotitle = `Projects | Indiealistic Studio - Ainul.tech`;
  const seodescrip = `Indiealistic is a digital strategy and interface design studio based in Pekanbaru City.`;
  const seourl = `https://ahmad.ainul.tech/projects/indiealistic`;
  const seopreviewimg = "/static/images/projects/indiealistic-feature-img.png";

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-x-2 space-y-4">
        <NextLink href="/">
          <a>
            <div className="inline-flex items-center space-x-1 text-gray-400 dark:text-gray-500 hover:text-black dark:hover:text-white text-base uppercase font-sourcecodepro">
              <p>Front-Page</p>
            </div>
          </a>
        </NextLink>
        <NextLink href="/projects">
          <a>
            <div className="inline-flex items-center space-x-1 text-gray-400 dark:text-gray-500 hover:text-black dark:hover:text-white text-base uppercase font-sourcecodepro">
              <p>/</p>
              <p>PRJ</p>
            </div>
          </a>
        </NextLink>
        <div className="inline-flex items-center space-x-1 text-black dark:text-white text-base uppercase font-sourcecodepro">
          <p>/</p>
          <h1>INDIEALISTIC</h1>
        </div>
      </div>

      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <div className="border-2 border-gray-200 dark:border-gray-700 p-6 pt-8">
          <div className="w-full flex items-center justify-center relative">
            <div className="inline-flex px-2 py-1 bg-black dark:bg-white absolute transform -translate-y-8">
              <p className="text-white dark:text-black font-sourcecodepro uppercase">
                Indiealistic Studio
              </p>
            </div>
          </div>
          <div className="space-y-2">
            <div className="w-full flex items-center justify-center">
              <VictoryHand className="text-black dark:text-white" size={25} />
            </div>
            <p className="text-black dark:text-white font-bold text-lg text-center">
              Propelling high-impact businesses
            </p>
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4 pt-4">
              <div className="flex items-center justify-center transform scale-[0.65] md:scale-[0.75] origin-center md:origin-top -mt-20 md:-mt-0 -mb-10 md:-mb-20">
                <INDCoin />
              </div>
              <div className="text-black dark:text-white col-span-2 space-y-4 pt-4">
                <div className="flex items-center space-x-4">
                  <div>
                    <RockOn size={25} />
                  </div>
                  <p className="text-sm">
                    Analyze external and internal trends, and your business’s
                    data to make optimal decisions.
                  </p>
                </div>
                <div className="flex items-center space-x-4">
                  <div>
                    <Plant size={25} />
                  </div>
                  <p className="text-sm">
                    Connect with your audience through any device, grow your
                    business with a web experience or an app.
                  </p>
                </div>
                <div className="flex items-center space-x-4">
                  <div>
                    <Inbox size={25} />
                  </div>
                  <p className="text-sm">
                    Develop fast, scalable and attractive technologies to
                    inspire the communities around them.
                  </p>
                </div>
                <div className="flex items-center space-x-4">
                  <div>
                    <Heart size={25} />
                  </div>
                  <p className="text-sm">
                    Grow your sales with communication strategies, creative
                    content and paid advertising.
                  </p>
                </div>
              </div>
            </div>
            <div className="w-full flex items-center justify-center py-4">
              <a href="https://indiealistic.studio">
                <div className="p-2 inline-flex items-center space-x-1 border-2 border-black dark:border-white">
                  <p className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                    INDIEALISTIC.STUDIO
                  </p>
                  <ArrowUpRight
                    className="text-black dark:text-white"
                    size={22}
                  />
                </div>
              </a>
            </div>
            <div className="space-y-4">
              <div className="flex items-center justify-center">
                <PointingUp
                  className="text-black dark:text-white animate-bounce"
                  size={22}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center justify-center">
          <MoreHorizontalFill
            className="text-black dark:text-white"
            size={22}
          />
        </div>
        <div className="border-2 border-black dark:border-white">
          <div>
            <div className="w-full flex border-b-2 border-black dark:border-white">
              <div className="w-full-20 border-r-2 border-black dark:border-white">
                <div className="p-2 border-black dark:border-white border-b-2">
                  <p className="text-black dark:text-white uppercase text-xs">
                    Logo
                  </p>
                </div>
                <div className="flex items-center justify-between">
                  <div className="w-full aspect-w-1 aspect-h-1 origin-center transform scale-75 object-cover relative">
                    <img
                      src="/logos/indiealistic-logo.svg"
                      className="absolute"
                    />
                  </div>
                </div>
              </div>
              <div className="w-full-80">
                <div className="border-b-2 border-black dark:border-white">
                  <div className="grid grid-cols-2">
                    <div className="text-black dark:text-white text-xs uppercase p-2 border-r-2 border-black dark:border-white">
                      {t("projects-sheet-code")} <strong>#STDINDIE</strong>
                    </div>
                    <div className="text-black dark:Text-white text-xs uppercase p-2">
                      {t("projects-sheet-start")} <strong>12/2019</strong>
                    </div>
                  </div>
                </div>

                <div className="border-b-2 border-black dark:border-white">
                  <div className="text-black dark:text-white text-xs uppercase p-2">
                    {t("projects-sheet-name")}
                  </div>
                  <div className="px-2 pb-2">
                    <h2 className="text-black dark:text-white font-bold text-center">
                      {t("project-indiealistic-title")}
                    </h2>
                  </div>
                </div>

                <div className="border-b-2 border-black dark:border-white">
                  <div className="text-black dark:text-white text-xs uppercase p-2">
                    Founders:
                  </div>
                  <div className="flex items-center justify-center">
                    <div className="px-2 pb-2">
                      <p className="text-black dark:text-white font-sourcecodepro text-base uppercase">
                        @ainul
                      </p>
                    </div>
                  </div>
                </div>

                <div>
                  <div className="text-black dark:text-white text-xs uppercase p-2">
                    {t("projects-sheet-description")}
                  </div>
                  <div className="px-2 pb-2">
                    <p className="text-black dark:text-white text-sm text-center">
                      {t("project-indiealistic-description")}
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div className="grid grid-cols-2 border-b-2 border-black dark:border-white">
              <div className="text-black dark:text-white text-xs uppercase p-2 border-r-2 border-black dark:border-white">
                {t("projects-sheet-status")}
                <div className="py-2 px-2 space-y-2">
                  <Developing />
                  <Configuration />
                </div>
              </div>
              <div className="text-black dark:text-white text-xs uppercase p-2">
                {t("projects-sheet-position")}
                <div className="py-2 px-2 space-y-2">
                  <Designer />
                  <Developer />
                </div>
              </div>
            </div>

            <div className="p-4 grid grid-cols-2 md:grid-cols-4 gap-4">
              <a
                href="https://indiealistic.studio"
                target="_blank"
                rel="noreferrer"
              >
                <div className="text-black dark:text-white uppercase text-sm border_dash_animated dark:border_dash_animated_dark p-2 transform transition-all hover:scale-105">
                  <Globe className="mb-1" />
                  Website
                </div>
              </a>
              <a
                href="mailto:hey@indiealistic.studio"
                target="_blank"
                rel="noreferrer"
              >
                <div className="text-black dark:text-white uppercase text-sm border_dash_animated dark:border_dash_animated_dark p-2 transform transition-all hover:scale-105">
                  <Envelope className="mb-1" />
                  Mail
                </div>
              </a>
            </div>
          </div>
        </div>

        <div className="w-full">
          <Pencil className="text-black dark:text-white mx-auto" />
        </div>

        <div className="w-full mx-auto">
          <LetterByAinul letterNumber="#0003">
            <div className="text-black dark:text-white text-sm">
              {" "}
              {t("project-indiealistic-letter-p1")}
              <br />
              <br />
              {t("project-indiealistic-letter-p2")}
              <br />
              <br />
              {t("project-indiealistic-letter-p3")}
            </div>
          </LetterByAinul>
        </div>
      </div>
    </Container>
  );
}
