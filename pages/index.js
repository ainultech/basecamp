import Container from "../components/Container";
import BlogSection from "../components/structures/BlogSection";
import ConceptsSection from "../components/structures/ConceptsSection";
import HeroSection from "../components/structures/HeroSection";
import InspirationSection from "../components/structures/InspirationSection";
import ProjectsSection from "../components/structures/ProjectsSection";
import StuffSection from "../components/structures/StuffSection";
import SubPageSection from "../components/structures/SubPageSection";
import TwitterSEO from "../components/TwitterSEO";
import ProductsSection from "../components/structures/ProductsSection";
import { Client } from "@notionhq/client";
import BlockchainSection from "../components/structures/BlockchainSection";
import TimelineSection from "../components/structures/TimelineSection";
import ProductHuntSection from "../components/structures/ProductHuntSection";
import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { GET_TOP_10_UPVOTES_OF_AINUL } from "../graphql/ProductHuntQuery";
import { setContext } from "apollo-link-context";
import Covid19Section from "../components/structures/Covid19Section";

export async function getStaticProps() {
  const notion = new Client({ auth: process.env.NOTION_API_OFFICIAL_KEYS });
  const databaseQuotesId = process.env.NOTION_PAGE_ID_QUOTES_PAGE;
  const responseQuotes = await notion.databases.query({
    database_id: databaseQuotesId,
  });

  const apiUrlOpenSea = process.env.OPENSEA_API_URL;
  const apiOptionOpenSea = { method: "GET" };
  const apiDataOpenSea = await fetch(apiUrlOpenSea, apiOptionOpenSea);
  const dataFromOpenSeaAPI = await apiDataOpenSea.json();

  const apiUrlStampsNotAlienOpenSea =
    process.env.OPENSEA_API_URL_STAMPS_NOTALIEN;
  const apiStampsNotAlienOptionOpenSea = { method: "GET" };
  const apiDataStampsNotAlienOpenSea = await fetch(
    apiUrlStampsNotAlienOpenSea,
    apiStampsNotAlienOptionOpenSea
  );
  const dataFromStampsNotAlienOpenSeaAPI =
    await apiDataStampsNotAlienOpenSea.json();

  const apiUrlCovid19Global = process.env.COVID_19_GLOBAL_API_URL;
  const apiOptionCovid19Global = { method: "GET" };
  const apiDataCovid19Global = await fetch(
    apiUrlCovid19Global,
    apiOptionCovid19Global
  );
  const dataFromCovid19GlobalAPI = await apiDataCovid19Global.json();

  const apiUrlCovid19Indonesia = process.env.COVID_19_INDONESIA_API_URL;
  const apiOptionCovid19Indonesia = { method: "GET" };
  const apiDataCovid19Indonesia = await fetch(
    apiUrlCovid19Indonesia,
    apiOptionCovid19Indonesia
  );
  const dataFromCovid19IndonesiaAPI = await apiDataCovid19Indonesia.json();

  // PRODUCT HUNT API FETCHING
  const authLink = setContext((_, { headers }) => {
    const token = process.env.PRODUCTHUNT_TOKEN;
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : "",
      },
    };
  });

  const httpLink = createHttpLink({
    uri: process.env.PRODUCTHUNT_API_ENDPOINT,
  });

  const clientPH = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
  });

  const { data } = await clientPH.query({
    query: GET_TOP_10_UPVOTES_OF_AINUL,
  });

  // COVID19 VACCINE DATA FETCHING
  const apiUrlCovid19VaccineIndonesia =
    process.env.COVID_19_VACCINE_INDONESIA_API_URL;
  const request = require("request");
  const csv = require("csvtojson");
  const apiDataCovid19VaccineIndonesia = await csv().fromStream(
    request.get(apiUrlCovid19VaccineIndonesia)
  );

  return {
    props: {
      resultsQuotes: responseQuotes.results,
      resultsOpenSeaAssets: dataFromOpenSeaAPI.assets,
      resultsStampsNotAlienOpenSeaAssets:
        dataFromStampsNotAlienOpenSeaAPI.assets,
      resultsCovid19Global: dataFromCovid19GlobalAPI,
      resultsCovid19Indonesia: dataFromCovid19IndonesiaAPI,
      resultsCovid19VaccineIndonesia: apiDataCovid19VaccineIndonesia,
      resultsProductHuntDatas: data.user.votedPosts.edges,
    },
    revalidate: 60,
  };
}

export default function Home({
  resultsQuotes,
  resultsOpenSeaAssets,
  resultsStampsNotAlienOpenSeaAssets,
  resultsCovid19Global,
  resultsCovid19Indonesia,
  resultsCovid19VaccineIndonesia,
  resultsProductHuntDatas,
}) {
  const seotitle = `AINUL.TECH - Ahmad Ainul's personal website`;
  const seodescrip = `Halaman pribadi Ahmad Ainul.R. Dibangun dengan Next.js / TailwindCSS dan dihosting di Vercel. Tempat untuk berbagi proyek, blog, dan hal-hal keren lainnya.`;
  const seourl = `https://ahmad.ainul.tech`;
  const seopreviewimg = "/static/ainul-tech-feature-img.png";

  return (
    <Container>
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <div className="mb-7 mt-4">
          <HeroSection />
        </div>
        <InspirationSection />
        <ProductsSection />
        <ProjectsSection />
        <BlogSection />
        <ProductHuntSection data={resultsProductHuntDatas} />
        <SubPageSection />
        <TimelineSection />
        <ConceptsSection />
        <BlockchainSection
          data={resultsOpenSeaAssets}
          dataStampsNotAlien={resultsStampsNotAlienOpenSeaAssets}
        />
        <Covid19Section
          dataGlobal={resultsCovid19Global}
          dataIndonesia={resultsCovid19Indonesia}
          dataVaccineIndonesia={resultsCovid19VaccineIndonesia}
        />
        <StuffSection resultssync={resultsQuotes} />
      </div>
    </Container>
  );
}
