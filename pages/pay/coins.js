import HideButton from "../../components/structures/pay/HideButton";
import PayFooter from "../../components/structures/pay/PayFooter";
import PayHeader from "../../components/structures/pay/PayHeader";
import PayNavigationBar from "../../components/structures/pay/PayNavigationBar";
import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { Copy } from "akar-icons";
import toast, { Toaster } from "react-hot-toast";

export default function Coins() {
  const [isAllCopied1, setIsAllCopied1] = useState(false);
  const [isAllCopied2, setIsAllCopied2] = useState(false);
  const [isAllCopied3, setIsAllCopied3] = useState(false);
  const [isAllCopied4, setIsAllCopied4] = useState(false);
  const [isAllCopied5, setIsAllCopied5] = useState(false);

  const onCopyAllText1 = () => {
    setIsAllCopied1(true);
    setTimeout(() => {
      setIsAllCopied1(false);
    }, 3000);
  };

  const onCopyAllText2 = () => {
    setIsAllCopied2(true);
    setTimeout(() => {
      setIsAllCopied2(false);
    }, 3000);
  };

  const onCopyAllText3 = () => {
    setIsAllCopied3(true);
    setTimeout(() => {
      setIsAllCopied3(false);
    }, 3000);
  };

  const onCopyAllText4 = () => {
    setIsAllCopied4(true);
    setTimeout(() => {
      setIsAllCopied4(false);
    }, 3000);
  };

  const onCopyAllText5 = () => {
    setIsAllCopied5(true);
    setTimeout(() => {
      setIsAllCopied5(false);
    }, 3000);
  };

  return (
    <div className="p-4">
      <PayHeader />
      <PayNavigationBar />
      <HideButton />
      <div className="items-start mx-auto w-full max-w-xl mt-7 space-y-6">
        <Toaster />
        <div className="text-black px-4 py-2 rounded-lg space-y-2">
          <div className="flex items-center space-x-4 w-full">
            <img src="/icons/bitcoin.svg" className="w-10 h-10 text-black" />
            <div className="w-[70%] md:w-[80%]">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Bitcoin
              </h3>
              <p className="text-black dark:text-white text-sm">
                bc1qhdhs6kl...2nvgj24adj6
              </p>
            </div>
            <div className="w-[30%] md:w-[20%]">
              <CopyToClipboard
                text="bc1qhdhs6klpuj5qqnh3ay9msyavf3t2nvgj24adj6"
                onCopy={onCopyAllText1}
              >
                <div className="p-2 bg-gray-100 dark:bg-gray-800 block items-center space-y-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                  <Copy
                    className={`text-black dark:text-white mx-auto ${
                      isAllCopied1 ? "text-purple-400" : "text-black"
                    }`}
                    size={22}
                  />
                  <p
                    className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                      isAllCopied1 ? "text-purple-400" : "text-black"
                    }`}
                  >
                    {isAllCopied1 ? "Copied" : "Copy"}
                  </p>
                </div>
              </CopyToClipboard>
            </div>
          </div>
          <div className="flex items-center space-x-4 w-full">
            <img src="/icons/ethereum.svg" className="w-10 h-10 text-black" />
            <div className="w-[70%] md:w-[80%]">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Ethereum
              </h3>
              <p className="text-black dark:text-white text-sm">
                0xE9e6Af6B3...b3C739B83b0
              </p>
            </div>
            <div className="w-[30%] md:w-[20%]">
              <CopyToClipboard
                text="0xE9e6Af6B3b011ab4C84D375f3061Bb3C739B83b0"
                onCopy={onCopyAllText2}
              >
                <div className="p-2 bg-gray-100 dark:bg-gray-800 block items-center space-y-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                  <Copy
                    className={`text-black dark:text-white mx-auto ${
                      isAllCopied2 ? "text-purple-400" : "text-black"
                    }`}
                    size={22}
                  />
                  <p
                    className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                      isAllCopied2 ? "text-purple-400" : "text-black"
                    }`}
                  >
                    {isAllCopied2 ? "Copied" : "Copy"}
                  </p>
                </div>
              </CopyToClipboard>
            </div>
          </div>
          <div className="flex items-center space-x-4 w-full">
            <img src="/icons/stacks.svg" className="w-10 h-10 text-black" />
            <div className="w-[70%] md:w-[80%]">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Stacks
              </h3>
              <p className="text-black dark:text-white text-sm">
                SP32TE16T6E...0K7EKETRPY8
              </p>
            </div>
            <div className="w-[30%] md:w-[20%]">
              <CopyToClipboard
                text="SP32TE16T6EZ4P02FD8JACY4FTKEH90K7EKETRPY8"
                onCopy={onCopyAllText3}
              >
                <div className="p-2 bg-gray-100 dark:bg-gray-800 block items-center space-y-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                  <Copy
                    className={`text-black dark:text-white mx-auto ${
                      isAllCopied3 ? "text-purple-400" : "text-black"
                    }`}
                    size={22}
                  />
                  <p
                    className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                      isAllCopied3 ? "text-purple-400" : "text-black"
                    }`}
                  >
                    {isAllCopied3 ? "Copied" : "Copy"}
                  </p>
                </div>
              </CopyToClipboard>
            </div>
          </div>
          <div className="flex items-center space-x-4 w-full">
            <img src="/icons/dogecoin.svg" className="w-10 h-10 text-black" />
            <div className="w-[70%] md:w-[80%]">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Dogecoin
              </h3>
              <p className="text-black dark:text-white text-sm">
                DJpvXdVDTDA...3hZr3EHd9Qw
              </p>
            </div>
            <div className="w-[30%] md:w-[20%]">
              <CopyToClipboard
                text="DJpvXdVDTDA1AncqgmcNHY53hZr3EHd9Qw"
                onCopy={onCopyAllText4}
              >
                <div className="p-2 bg-gray-100 dark:bg-gray-800 block items-center space-y-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                  <Copy
                    className={`text-black dark:text-white mx-auto ${
                      isAllCopied4 ? "text-purple-400" : "text-black"
                    }`}
                    size={22}
                  />
                  <p
                    className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                      isAllCopied4 ? "text-purple-400" : "text-black"
                    }`}
                  >
                    {isAllCopied4 ? "Copied" : "Copy"}
                  </p>
                </div>
              </CopyToClipboard>
            </div>
          </div>
          <div className="flex items-center space-x-4 w-full">
            <img src="/icons/stellar.svg" className="w-10 h-10 text-black" />
            <div className="w-[70%] md:w-[80%]">
              <h3 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
                Stellar
              </h3>
              <p className="text-black dark:text-white text-sm">
                GDGI3HLNK63...TU3LJ6DGGQH
              </p>
            </div>
            <div className="w-[30%] md:w-[20%]">
              <CopyToClipboard
                text="GDGI3HLNK637MH44JHB3WFJIJBQUAQSSYYDRVPOZ43ZZ7TU3LJ6DGGQH"
                onCopy={onCopyAllText5}
              >
                <div className="p-2 bg-gray-100 dark:bg-gray-800 block items-center space-y-2 rounded-lg justify-center cursor-pointer transform transition-all active:scale-75">
                  <Copy
                    className={`text-black dark:text-white mx-auto ${
                      isAllCopied5 ? "text-purple-400" : "text-black"
                    }`}
                    size={22}
                  />
                  <p
                    className={`text-black dark:text-white font-sourcecodepro text-sm md:text-sm uppercase text-center ${
                      isAllCopied5 ? "text-purple-400" : "text-black"
                    }`}
                  >
                    {isAllCopied5 ? "Copied" : "Copy"}
                  </p>
                </div>
              </CopyToClipboard>
            </div>
          </div>
        </div>
      </div>
      <PayFooter />
    </div>
  );
}
