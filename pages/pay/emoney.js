import { Copy } from "akar-icons";
import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import ExternalLink from "../../components/elements/ExternalLink";
import HideButton from "../../components/structures/pay/HideButton";
import PayFooter from "../../components/structures/pay/PayFooter";
import PayHeader from "../../components/structures/pay/PayHeader";
import PayNavigationBar from "../../components/structures/pay/PayNavigationBar";

export default function Emoney() {
  const [isAllCopied1, setIsAllCopied1] = useState(false);

  const [isAllCopied2, setIsAllCopied2] = useState(false);

  const [isAllCopied3, setIsAllCopied3] = useState(false);

  const [isAllCopied4, setIsAllCopied4] = useState(false);

  const [isAllCopied5, setIsAllCopied5] = useState(false);

  const onCopyAllText1 = () => {
    setIsAllCopied1(true);
    setTimeout(() => {
      setIsAllCopied1(false);
    }, 3000);
  };

  const onCopyAllText2 = () => {
    setIsAllCopied2(true);
    setTimeout(() => {
      setIsAllCopied2(false);
    }, 3000);
  };

  const onCopyAllText3 = () => {
    setIsAllCopied3(true);
    setTimeout(() => {
      setIsAllCopied3(false);
    }, 3000);
  };

  const onCopyAllText4 = () => {
    setIsAllCopied4(true);
    setTimeout(() => {
      setIsAllCopied4(false);
    }, 3000);
  };

  const onCopyAllText5 = () => {
    setIsAllCopied5(true);
    setTimeout(() => {
      setIsAllCopied5(false);
    }, 3000);
  };

  return (
    <div className="p-4">
      <PayHeader />
      <PayNavigationBar />
      <HideButton />
      <div className="items-start mx-auto w-full max-w-xl mt-7 space-y-6">
        <div className="w-full border-2 border-black dark:border-white">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
            <div className="border-r-0 md:border-r-2 border-b-2 md:border-b-0 border-black dark:border-white">
              <div className="w-full h-full grid grid-cols-2 gap-2">
                <div className="border-r-2 h-full border-black dark:border-white">
                  <div className="p-2 h-full flex items-center justify-center">
                    <div className="w-10 h-10 relative">
                      <img
                        src="/icons/icon-bmc.svg"
                        className="object-cover absolute"
                      />
                    </div>
                  </div>
                </div>
                <div className="p-2 flex items-center justify-center">
                  <h3 className="text-black dark:text-white uppercase text-base font-sourcecodepro text-center">
                    USD
                  </h3>
                </div>
              </div>
            </div>
            <div className="font-sourcecodepro px-4 py-2 md:p-2 flex w-full items-center">
              <div className="w-[76%]">
                <h3 className="text-black dark:text-white text-base">
                  Buy Me a Coffee
                </h3>
              </div>
              <div className="w-[24%]">
                <div className="p-2 bg-gray-800 block items-center space-y-2 rounded-lg justify-center">
                  <ExternalLink
                    href="https://www.buymeacoffee.com/ainultech"
                    colortexthover="rainbow_text_hover"
                    colortext="text-white"
                  >
                    LINK
                  </ExternalLink>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="w-full border-2 border-black dark:border-white">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
            <div className="border-r-0 md:border-r-2 border-b-2 md:border-b-0 border-black dark:border-white">
              <div className="w-full h-full grid grid-cols-2 gap-2">
                <div className="border-r-2 h-full border-black dark:border-white">
                  <div className="p-2 h-full flex items-center justify-center">
                    <div className="w-10 h-10 relative">
                      <img
                        src="/icons/icon-ko-fi.svg"
                        className="object-cover absolute"
                      />
                    </div>
                  </div>
                </div>
                <div className="p-2 flex items-center justify-center">
                  <h3 className="text-black dark:text-white uppercase text-base font-sourcecodepro text-center">
                    USD
                  </h3>
                </div>
              </div>
            </div>
            <div className="font-sourcecodepro px-4 py-2 md:p-2 flex w-full items-center">
              <div className="w-[76%]">
                <h3 className="text-black dark:text-white text-base">Ko-fi</h3>
              </div>
              <div className="w-[24%]">
                <div className="p-2 bg-gray-800 block items-center space-y-2 rounded-lg justify-center">
                  <ExternalLink
                    href="https://ko-fi.com/ainultech"
                    colortexthover="rainbow_text_hover"
                    colortext="text-white"
                  >
                    LINK
                  </ExternalLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <PayFooter />
    </div>
  );
}
