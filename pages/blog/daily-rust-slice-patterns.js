import Container from "../../components/Container";
import Paragraph from "../../components/elements/block/Paragraph";
import Heading2 from "../../components/elements/block/Heading2";
import { NextSeo } from "next-seo";
import dodont from "@ainultech/dodont";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { twilight } from "react-syntax-highlighter/dist/cjs/styles/prism";
import ExternalLink from "../../components/elements/ExternalLink";
import CallOut from "../../components/elements/block/CallOut";
import TwitterSEO from "../../components/TwitterSEO";
import EndPostSection from "../../components/structures/EndPostSection";
import BlogHeader from "../../components/structures/BlogHeader";
import Comments from "../../components/Comments";

export default function daily_rust_slice_patterns() {
  const seotitle = `Daily Rust: Slice Patterns`;
  const seodescrip = `Pattern matching in Rust works by checking if a place in memory (the "data") matches a certain pattern. In this post, we will look at some recent improvements to patterns soon available in stable Rust as well as some more already available in nightly.`;
  const seourl = `https://ahmad.ainul.tech/blog/daily-rust-slice-patterns`;
  const seopreviewimg =
    "/static/images/daily-rust-slice-patterns/feature-img.png";
  const headerslug = "daily-rust-slice-patterns";
  const headerpublish = "2021-08-14";
  const headerreadingtime = "3";
  const headerlanguage = "English";
  const headerfeatureimggif =
    "/static/images/daily-rust-slice-patterns/header-img.gif";
  const headerauthors = "Ahmad Ainul Rizki";

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <BlogHeader
          title={seotitle}
          publishedat={headerpublish}
          readingtime={headerreadingtime}
          language={headerlanguage}
          viewcounterslug={headerslug}
          description={seodescrip}
          featureimggif={headerfeatureimggif}
          authors={headerauthors}
        />
        <div className="text-black dark:text-white space-y-3">
          <Paragraph>
            <ExternalLink
              href="https://blog.rust-lang.org/2018/05/10/Rust-1.26.html#basic-slice-patterns"
              colortexthover="rainbow_text_hover"
            >
              Rust 1.26
            </ExternalLink>{" "}
            introduced a nifty little feature called Basic Slice Patterns which
            lets you pattern match on slices with a known length. Later on in{" "}
            <ExternalLink
              href="https://blog.rust-lang.org/2020/03/12/Rust-1.42.html#subslice-patterns"
              colortexthover="rainbow_text_hover"
            >
              Rust 1.42
            </ExternalLink>
            , this was extended to allow using <code>..</code> to match on
            “everything else”.
          </Paragraph>
          <Paragraph>
            As features go this may seem like a small addition, but it gives
            developers an opportunity to write much more expressive code.
          </Paragraph>
          <CallOut emoji="💡" bgcolor="bg-yellow-50 dark:bg-yellow-800">
            The code written in this article is available in the various
            playground links dotted throughout. Feel free to browse through and
            steal code or inspiration.
            <br />
            <br />
            If you found this useful or spotted a bug in the article, let me
            know on the blog’s issue tracker!
          </CallOut>
          <Heading2>Handling Plurality</Heading2>
          <Paragraph>
            One of the simplest applications of slice patterns is to provide
            user-friendly messages by matching on fixed length slices.
          </Paragraph>
          <Paragraph>
            Often it’s nice to be able to customise your wording depending on
            whether there were 0, 1, or many items. For example, this snippet…
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="rust"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            fn print_words(sentence: &str) {
                let words: Vec<_> = sentence.split_whitespace().collect();

                match words.as_slice() {
                    [] => println!("There were no words"),
                    [word] => println!("Found 1 word: {}", word),
                    _ => println!("Found {} words: {:?}", words.len(), words),
                }
            }

            fn main() {
                print_words("");
                print_words("Hello");
                print_words("Hello World!");
            }
          `}
          </SyntaxHighlighter>
          <ExternalLink
            href="https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=b5f39a8f3b759134bc1b5f1ccf71b58e"
            colortexthover="rainbow_text_hover"
          >
            (playground)
          </ExternalLink>
          <Paragraph>… will generate this output:</Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="text"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            There were no words
            Found 1 word: Hello
            Found 2 words: ["Hello", "World!"]
          `}
          </SyntaxHighlighter>
          <Heading2>Matching the Start of a Slice</Heading2>
          <Paragraph>
            The <code>..</code> syntax is called a “rest” pattern and lets you
            match on (surprise, surprise) the rest of the slice.
          </Paragraph>
          <Paragraph>
            According to the{" "}
            <ExternalLink
              href="https://en.wikipedia.org/wiki/Executable_and_Linkable_Format"
              colortexthover="rainbow_text_hover"
            >
              ELF Format
            </ExternalLink>
            , all ELF binaries must start with the sequence{" "}
            <code>0x7f ELF</code>. We can use this fact and rest patterns to
            implement our <code>own is_elf()</code> check.
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="rust"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            use std::error::Error;

            fn is_elf(binary: &[u8]) -> bool {
                match binary {
                    [0x7f, b'E', b'L', b'F', ..] => true,
                    _ => false,
                }
            }

            fn main() -> Result<(), Box<dyn Error>> {
                let current_exe = std::env::current_exe()?;
                let binary = std::fs::read(&current_exe)?;

                if is_elf(&binary) {
                    print!("{} is an ELF binary", current_exe.display());
                } else {
                    print!("{} is NOT an ELF binary", current_exe.display());
                }

                Ok(())
            }
          `}
          </SyntaxHighlighter>
          <ExternalLink
            href="https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=f26b605fc432a06fb062ebe56fee289f"
            colortexthover="rainbow_text_hover"
          >
            (playground)
          </ExternalLink>
          <Heading2>Checking for Palindromes</Heading2>
          <Paragraph>
            A very common introductory challenge in programming is to write a
            check for palindromes.
          </Paragraph>
          <Paragraph>
            We can use the fact that the <code>@</code> symbols binds a new
            variable to whatever it matches, and our ability to match on both
            the start and end of a slice to create a particularly elegant{" "}
            <code>is_palindrome()</code> function.
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="rust"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            fn is_palindrome(items: &[char]) -> bool {
                match items {
                    [first, middle @ .., last] => first == last && is_palindrome(middle),
                    [] | [_] => true,
                }
            }
          `}
          </SyntaxHighlighter>
          <ExternalLink
            href="https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=baeec729aea945d2cd98387d1333ba8f"
            colortexthover="rainbow_text_hover"
          >
            (playground)
          </ExternalLink>
          <Heading2>A Poor Man’s Argument Parser</Heading2>
          <Paragraph>
            Another way you might want to use slice patterns is by “peeling off”
            desired prefixes or suffixes.
          </Paragraph>
          <Paragraph>
            Although more sophisticated crates like <code>clap</code> and{" "}
            <code>structopt</code> exist, we can use this to implement our own
            basic argument parser.
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="rust"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            fn parse_args(mut args: &[&str]) -> Args {
                let mut input = String::from("input.txt");
                let mut count = 0;

                loop {
                    match args {
                        ["-h" | "--help", ..] => {
                            eprintln!("Usage: main [--input <filename>] [--count <count>] <args>...");
                            std::process::exit(1);
                        }
                        ["-i" | "--input", filename, rest @ ..] => {
                            input = filename.to_string();
                            args = rest;
                        }
                        ["-c" | "--count", c, rest @ ..] => {
                            count = c.parse().unwrap();
                            args = rest;
                        }
                        [..] => break,
                    }
                }

                let positional_args = args.iter().map(|s| s.to_string()).collect();

                Args {
                    input,
                    count,
                    positional_args,
                }
            }

            struct Args {
                input: String,
                count: usize,
                positional_args: Vec<String>,
            }
          `}
          </SyntaxHighlighter>
          <ExternalLink
            href="https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=aa016782dab527e80014c932fb769734"
            colortexthover="rainbow_text_hover"
          >
            (playground)
          </ExternalLink>
          <Heading2>Irrefutable Pattern Matching</Heading2>
          <Paragraph>
            Although not technically part of the Slice Patterns feature, you can
            use pattern matching to destructure fixed arrays outside of a{" "}
            <code>match</code> or <code>if let</code> statement.
          </Paragraph>
          <Paragraph>
            This can be useful in avoiding clunkier sequences based on indices
            which will never fail.
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="rust"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            fn format_coordinates([x, y]: [f32; 2]) -> String {
                format!("{}|{}", x, y)
            }

            fn main() {
                let point = [3.14, -42.0];

                println!("{}", format_coordinates(point));

                let [x, y] = point;
                println!("x: {}, y: {}", x, y);
                // Much more ergonomic than writing this!
                // let x = point[0];
                // let y = point[1];
            }
          `}
          </SyntaxHighlighter>
          <ExternalLink
            href="https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=dfbcc3a1bcf3545e3a15fedd57abe8cd"
            colortexthover="rainbow_text_hover"
          >
            (playground)
          </ExternalLink>
          <Heading2>Conclusions</Heading2>
          <Paragraph>
            As far as features go in Rust slice patterns aren’t overly complex
            but when used appropriately, they can really improve the
            expressiveness of your code.
          </Paragraph>
          <Paragraph>
            This was a lot shorter than my usual deep dives, but hopefully you
            learned something new. Going forward I’m hoping to create more of
            these Daily Rust posts, copying shamelessly from Jonathan Boccara’s{" "}
            <ExternalLink
              href="https://www.fluentcpp.com/2017/04/04/the-dailies-a-new-way-to-learn-at-work/"
              colortexthover="rainbow_text_hover"
            >
              Daily C++
            </ExternalLink>
            .
          </Paragraph>
        </div>
        <EndPostSection telegramid="4">
          <Comments telegramdiscussurl="ainulchannel/4" />
        </EndPostSection>
      </div>
    </Container>
  );
}
