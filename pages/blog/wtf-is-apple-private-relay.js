import Container from "../../components/Container";
import Paragraph from "../../components/elements/block/Paragraph";
import Heading2 from "../../components/elements/block/Heading2";
import { NextSeo } from "next-seo";
import dodont from "@ainultech/dodont";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { twilight } from "react-syntax-highlighter/dist/cjs/styles/prism";
import ExternalLink from "../../components/elements/ExternalLink";
import BulletList from "../../components/elements/block/BulletList";
import TwitterSEO from "../../components/TwitterSEO";
import EndPostSection from "../../components/structures/EndPostSection";
import BlogHeader from "../../components/structures/BlogHeader";
import Comments from "../../components/Comments";

export default function wtf_is_apple_private_relay() {
  const seotitle = `WTF is Apple Private Relay?`;
  const seodescrip = `A summary of what we know about Apple Private Relay.`;
  const seourl = `https://ahmad.ainul.tech/blog/wtf-is-apple-private-relay`;
  const seopreviewimg =
    "/static/images/wtf-is-apple-private-relay/feature-img.png";
  const headerslug = "wtf-is-apple-private-relay";
  const headerpublish = "2021-08-31";
  const headerreadingtime = "11";
  const headerlanguage = "English";
  const headerfeatureimggif =
    "/static/images/wtf-is-apple-private-relay/header-img.jpg";
  const headerauthors = "Ahmad Ainul Rizki";

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <Heading2>What is Apple Private Relay?</Heading2>
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <BlogHeader
          title={seotitle}
          publishedat={headerpublish}
          readingtime={headerreadingtime}
          language={headerlanguage}
          viewcounterslug={headerslug}
          description={seodescrip}
          featureimggif={headerfeatureimggif}
          authors={headerauthors}
        />
        <div className="text-black dark:text-white space-y-3">
          <Paragraph>
            I want to share my perspective on this as I’ve seen many useful and
            very complex protocol implementations.
          </Paragraph>
          <Paragraph>
            First of all, if you’re not familiar with protocols yet I recommend
            to check out the{" "}
            <ExternalLink
              href="https://elixir-lang.org/getting-started/protocols.html"
              colortexthover="rainbow_text_hover"
            >
              official getting started guide for protocols
            </ExternalLink>
            . It’s a very good resource to learn about the topic and understand
            how it works. For the rest of this post, I’ll assume you understand
            what a protocol is and does. I don’t want to repeat the same
            material with the chance my version is a poor copy of the original.
            This also has the potential to leave you more clueless than when you
            started reading this article.
          </Paragraph>
          <Paragraph>
            The example given by the official guide is quite useful. It defines
            the protocol <code>Size</code> and it is defined like this:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="elixir"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            defprotocol Size do
              @doc "Calculates the size (and not the length!) of a data structure"
              def size(data)
            end
          `}
          </SyntaxHighlighter>
          <Paragraph>
            In fact, the intention of this protocol is not to <i>calculate</i>{" "}
            the size, but to return the pre-computed size of an element ().
            Perhaps, the protocol would be better named{" "}
            <code>PreComputedSize</code> to clarify intent, but it becomes a bit
            long and you could easily clarify intent in the module
            documentation. I think it is very important to describe what your
            protocol is supposed to do. I believe it makes it easier to add new
            implementations.
          </Paragraph>
          <Heading2>My Webshop</Heading2>
          <Paragraph>
            For the moment, let’s assume I own a webshop and I sell a couple of
            office supplies; paper & paperclips. When I need to ship these items
            I need to know an approximation of the volume so I know how many I
            can pack in a box. I’ll define a protocol called{" "}
            <code>Product</code> with a function <code>volume/1</code> and
            implement the protocol for <code>%Paper</code> and{" "}
            <code>%Paperclips{}</code>. Here is it in code:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="elixir"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            defprotocol Product do
              @doc "Calculates the volume of a data structure"
              def volume(term)
            end

            defimpl Product, for: Paperclips do
              def volume(paperclips) do
                paperclips.volume
              end
            end

            defimpl Product, for: Paper do
              def volume(paper) do
                paper.x * paper.y * paper.z
              end
            end
          `}
          </SyntaxHighlighter>
          <Paragraph>
            As you can see, the data structure for paperclips has a volume key
            and the protocol can just return the value. For paper, it is a bit
            more complex as for the dimensions multiplication is required, but
            it’s not too hard. Now when I get a list of items I can easily
            calculate the volume and pack everything in one or multiple boxes:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="elixir"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            items
            |> Enum.map(&Product.volume/1)
            |> pack_in_a_box()
          `}
          </SyntaxHighlighter>
          <Paragraph>
            And voila! I can sell paper &amp; paperclips via my webshop and pack
            them in appropriate boxes.
          </Paragraph>
          <Heading2>Adding a Product (or Two)</Heading2>
          <Paragraph>
            But to my surprise, the webshop has been{" "}
            <strong>a great success</strong> and I want to add a product. This
            should be fairly easy as I defined a protocol and{" "}
            <code>Product</code> can simply be implemented for{" "}
            <code>%Pencil{}</code>. Here we go:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="elixir"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            defimpl Product, for: Pencil do
              def volume(pencil) do
                pi() * :math.pow(pencil.r / 2, 2) * pencil.h
              end
            end
          `}
          </SyntaxHighlighter>
          <Paragraph>
            It’s probably a good idea not to calculate <code>pi/0</code> on the
            spot, but what if I would? From an implementation perspective, it
            seems to be correct. This is indeed how you calculate the volume for
            a pencil. However, suddenly my box packing code becomes slow at
            seemingly random moments. You can see that the protocol makes it
            easy to extend it. Though the complexity of the implementation may
            have unintended side-effects. We can still make it a bit worse
            though since I also plan on selling boxed pencils.
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            language="elixir"
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            defimpl Product, for: Pencilbox do
              def volume(pencilbox) do
                pencilbox.pencil_count * Product.volume(pencilbox.pencil)
              end
            end
          `}
          </SyntaxHighlighter>
          <Paragraph>
            <strong>Whoa! MIND. BLOWN.</strong> We’re not only achieving
            polymorphism but <em>recursive polymorphism</em>. While I will admit
            straight away there might be good use-cases for this, I haven’t seen
            one yet. In most real-world scenarios where you write common
            business logic, you don’t need this level of complexity. It will be
            hard to reason or understand my simple box packing code because of
            the underlying complexity that is hidden from the view.
          </Paragraph>
          <Heading2>
            Here Is My Advice When You’re Considering Creating a Protocol
          </Heading2>
          <BulletList>
            <li>
              <Paragraph>
                Describe your protocol as precise as you can
              </Paragraph>
            </li>
            <li>
              <Paragraph>
                Implementations for the same protocol should have similar
                complexity
              </Paragraph>
            </li>
            <li>
              <Paragraph>
                You probably don’t want recursive polymorphism
              </Paragraph>
            </li>
          </BulletList>
          <Paragraph>
            One last piece of advice, which is only useful if your protocol
            needs some helpers, is to wrap access to your protocol in a module
            and provide the helper functions from there. It is similar to the{" "}
            <code>Enumerable</code> protocol and the <code>Enum</code> module.
          </Paragraph>
          <Paragraph>
            Did you enjoy this blogpost? Do you have something to add? Perhaps
            you have a different opinion you’d like to share? I’d love to hear
            and get some feedback! You can find my social/email at the top of
            the page!
          </Paragraph>
          <Paragraph>
            Thank you Alexey G. for proof-reading this post!
          </Paragraph>
        </div>
        <EndPostSection telegramid="5">
          <Comments telegramdiscussurl="ainulchannel/5" />
        </EndPostSection>
      </div>
    </Container>
  );
}
