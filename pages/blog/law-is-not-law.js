import Container from "../../components/Container";
import Paragraph from "../../components/elements/block/Paragraph";
import Heading3 from "../../components/elements/block/Heading3";
import { NextSeo } from "next-seo";
import { VictoryHand } from "akar-icons";
import ExternalLink from "../../components/elements/ExternalLink";
import CallOut from "../../components/elements/block/CallOut";
import QuotePeople from "../../components/elements/block/QuotePeople";
import TwitterSEO from "../../components/TwitterSEO";
import EndPostSection from "../../components/structures/EndPostSection";
import BlogHeader from "../../components/structures/BlogHeader";
import Comments from "../../components/Comments";

export default function law_is_not_law() {
  const seotitle = `Law is Not Law`;
  const seodescrip = `A key feature of contemporary notions of code as law is the quality of being trustless. To better understand what this means, we must first consider exactly how trust is operative within conventional legal structures.`;
  const seourl = `https://ahmad.ainul.tech/blog/law-is-not-law`;
  const seopreviewimg = "/static/images/law-is-not-law/feature-img.png";
  const headerslug = "law-is-not-law";
  const headerpublish = "2021-02-26";
  const headerreadingtime = "30";
  const headerlanguage = "English";
  const headerfeatureimggif =
    "https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/3qj3VtNL2nhmw/giphy.gif";
  const headerauthors = "Ahmad Ainul Rizki";

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <BlogHeader
          title={seotitle}
          publishedat={headerpublish}
          readingtime={headerreadingtime}
          language={headerlanguage}
          viewcounterslug={headerslug}
          description={seodescrip}
          featureimggif={headerfeatureimggif}
          authors={headerauthors}
        />
        <div className="text-black dark:text-white space-y-3">
          <Paragraph>
            There is a conceit among the purveyors of decentralized, open, or
            otherwise distributed network protocols that <i>code is law</i>{" "}
            which, while often invoked in the negative sense of rationalizing
            the exploitation of vulnerabilities within code, has more lately
            come to posit more materially consequent implications.
            <a name="fnref:1" href="#fn:1" className="fgoto">
              1
            </a>{" "}
            Specifically, consensus as a mechanism of a protocol’s operation as
            well as its proliferation bears forth new meaning for{" "}
            <i>code as law</i>; as in, a protocol’s operation by and through the{" "}
            <i>prerendered</i> consent of those subject to that protocol
            approaches the formation of something more law-like than is actual{" "}
            <i>law</i>.
          </Paragraph>
          <Paragraph>
            Let us consider, for instance, that state legal formations emerge
            actualized only by means of coercion at the discretion of those
            monopolizing the means of coercion (application of violent force,
            wielding overwhelming force of arms, tactical posturing, etc.); such
            law bears no more guarantee of consistency or persistence than some
            despotic reign in a state of general anarchy. What sets some legal
            structures indubitably aloft others, however, is its totalitarian
            pervasion through materially conditioned enactments of mutual
            surveillance, gamesmanship, and particularly the ways in which these
            enactments play out within, rather than between individuals–an
            ideological framework, effectively automating away some portion of
            the <em>coercive burden of state-sanction</em> (sometimes referred
            to as the <em>punitive</em> or <em>disciplinary</em> sort which
            nevertheless must rear its head from time to time). One might well
            imagine that herein lies the distinction between the authoritarian
            injunction and that of the social (or even commercial, as it were)
            “manufacturing of consent”
            <a name="fnref:2" href="#fn:2" className="fgoto">
              2
            </a>{" "}
            within the complex of their effectively, albeit largely spontaneous
            totalitarian collusion.
          </Paragraph>
          <Paragraph>
            A key feature of contemporary notions of <em>code as law</em> is the
            quality of being <em>trustless</em>. To better understand what this
            means, we must first consider exactly how <em>trust</em> is
            operative within conventional legal structures. The peculiar
            dimensions of this dichotomy are illustrated quite well in a piece
            regarding the{" "}
            <ExternalLink
              href="https://en.wikipedia.org/wiki/Byzantine_fault"
              colortexthover="rainbow_text_hover"
            >
              “Byzantine Generals Problem”
            </ExternalLink>{" "}
            by Kei Kreutler.
          </Paragraph>
          <CallOut emoji="💡" bgcolor="bg-yellow-50 dark:bg-yellow-800">
            Blockchain technology claims to be trustless, in the sense that
            individuals don’t have to trust intermediaries or more powerful
            single actors in order to act and interact. The technical system
            off-loads authority onto a transparent and public consensus history,
            created and validated by the protocol and some of its users. The
            technical system could be considered a “trustless,” multiauthored
            actor in its own right, with the however unlikely and expensive
            scenario of its own validators’ collusion. “We don’t need to trust
            each other before we can begin to collaborate,” blockchain
            technology claims. Through the massively poor media reporting on
            blockchain, this claim can come to be misinterpreted, as today’s
            dominant connotation of trust suggests something interpersonal and
            chosen. The term “trustless” skims over that what may have been
            previously defined as “trust”—as in trusted institutions—and in many
            scenarios may not refer to voluntary, cultivated relationships but
            instead arise as a result of contingency and lack of alternatives.
            In this light, trust was the network effect of rumors around
            consolidation of power.
            <a name="fnref:3" href="#fn:3" className="fgoto">
              3
            </a>
          </CallOut>
          <Paragraph>
            Let us consider that commodification is perhaps the prevalent form
            of social mediation operative today. Marx most aptly referred to
            this phenomenon as <em>material relations between people</em> and{" "}
            <em>social relations between things</em> (we might also extend this
            to social relations between people <em>only as things</em> or, in
            other words, as themselves commodities of a sort); hence may we
            observe, in whatever various dealings and interactions people might
            engage, the implication of <em>trust</em>, but a trust in what if
            not in the other party’s embroilment in a common distance or
            alienation from the social course of transaction? It is a trust in
            the thoroughness of commodification, that a counterparty might prove
            similarly compelled to act only in capacity as arbiter of material
            relations so that the social relations of the commodity form or that
            of <em>the market</em> be simply allowed to <em>play out</em> (or
            merely imagined to do so), and all in effect that one might set
            aside more natural or even, to some extent, rational compulsions.
            (or that one’s habit of compulsion prove sufficiently denaturalized)
          </Paragraph>
          <Paragraph>
            Trustlessness, on the other hand, removes any need for whatever
            presumption of effectively enacted commodification by relegating the
            hypothetically smooth operating of a network of transaction (really
            just the consultation of the public ledger through the validation of
            its addenda) to deterministic mechanisms of consensus: breaches of
            trust only proving possible <em>off-network</em> or{" "}
            <em>off-chain</em> (misrepresentations only by intermediated
            obfuscation of what is otherwise its public and disintermediated
            process). To the extent that this is representative of code{" "}
            <em>as</em> law, we can see quite plainly the extent to which this
            paradigm begins to exceed and supplant any hypothetical code{" "}
            <em>of</em> law. This would apparently juxtapose our sense of the
            former as something of a referendum on the latter, casting doubt
            upon the mythic foundations of state and law
            <a name="fnref:4" href="#fn:4" className="fgoto">
              4
            </a>{" "}
            –upon its very <em>legitimacy</em>.
            <a name="fnref:5" href="#fn:5" className="fgoto">
              5
            </a>
          </Paragraph>
          <QuotePeople
            imgwhosaid="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/3qj3VtNL2nhmw/giphy.gif"
            namewhossaid="Friedrich Nietzsche"
            reflink="https://www.goodreads.com/book/show/51893.Thus_Spoke_Zarathustra"
            reftitle="Thus Spoke Zarathustra"
          >
            Where a people still exists, there the people do not understand the
            state and hate it as the evil eye and sin against custom and law.
          </QuotePeople>
          <Paragraph>
            Yet even some hypothetical succession or displacement of any older
            sense of law by such a radical innovation would not in and of itself
            decouple some newly emerging socio-legal formation from the
            comparatively <em>criminal</em> origins of its predecessor–that is,
            if we consider closely the most pernicious and persistent quality of
            said criminality in the <em>illegitimate</em> (which is to say,{" "}
            <em>merely retroactively assumed as legitimate</em>) agglomeration
            of resources, power, etc.. Even if we consider coercive
            capacity–assymetric application and monopolization of violence–as
            the defining feature or definitive means of all law-making and
            law-preserving, that very capacity emerges of definite historical
            moments of some consolidation of its means and the redistributive
            effect of violent upheaval emerging of the inner tension or the
            contradictions straining such moments to the point of rupture. And
            what are these means of coercion other than a primitive and
            subordinate means of social production? The redistributive rupture
            of which proving ultimately the cesarean delivery of a new mode of
            production more generally.
          </Paragraph>
          <Paragraph>
            Suffice to say, a transfer of power is seldom cleanly discernible
            from an accompanying transfer of wealth, and what else constitutes
            wealth other than the means to exploit productive power? Thus might
            we find the locus of power and the very object of{" "}
            <em>law as such</em> as merely or preeminently the reproductive
            capacity or productive mode by which some historical distribution of
            its means expresses itself, however obfuscated by the mythic origins
            of its accompanying legal apparatus, its instrumentation of the
            state.
          </Paragraph>
          <Paragraph>
            Given such incidental beginnings, law-as-such might be seen to
            suggest a strained and desperate sort of legitimacy–one garnered ad
            hoc and at great pains of subjection and destitution for those who
            find themselves under its heel. Comparatively, a programmatically
            deterministic elimination or minimization of counterparty risk,
            empirically auditable, and resting mechanistically upon a wide base
            of consensus might well strike us as rather securely legitimized
            within its narrow and relatively well-defined scope of mediation.
            Herein do we better glean the extent to which trust is operative
            within the <em>code of law</em> only as an abstraction of what
            amounts to a discipline of co-learned helplessness before some
            well-established injunction, rather than a common civic duty among
            free and dutiful subjects (certainly far from any free association
            between such subjects); hence, trustlessness within{" "}
            <em>code as law</em> is operative only in the degree to which any
            such “trust” be rendered unnecessary <em>in advance</em> or, rather,
            to the extent that legitimacy garnered by a specially coercive
            apparatus be displaced by that emerging from the commonplace
            coercion levied by society at large. (i.e., custom or social
            convention, or perhaps even approaching a more sophisticated sense
            of <em>protocol</em>)
          </Paragraph>
          <Heading3>
            [<em>The Expeditionary Tendency</em>]
          </Heading3>
          <Paragraph>
            <em>
              Here might we comprehend the discernibly political implications of
              any technical matter of consequence pursued to its end–to its
              final analysis. This manner of analysis should prove essential to
              any coherent formulation of what we might refer to as the{" "}
              <strong>expeditionary mode</strong> (or that of{" "}
              <strong>rapid</strong> expedition
              <a name="fnref:6" href="#fn:6" className="fgoto">
                6
              </a>
              ) and, thus, constitutes what we might refer to as the{" "}
              <ExternalLink
                href="https://medium.com/weaponize/tagged/expeditionary-tendency"
                colortexthover="rainbow_text_hover"
              >
                _Expeditionary Tendency_
              </ExternalLink>
              . Implied here is the extent to which an orientation toward such a
              mode is necessarily of political consequence, if any at all.
            </em>
          </Paragraph>
          <Paragraph>
            <em>
              Hence shall we proceed under this distinction insofar as we
              concern ourselves with the broader scope of application around and
              within the practice of <strong>Rapid Expedition</strong>
              <a name="fnref:7" href="#fn:7" className="fgoto">
                7
              </a>{" "}
              as it might be seen to converge with the project of{" "}
              <strong>American Contra</strong>,
              <a name="fnref:8" href="#fn:8" className="fgoto">
                8
              </a>{" "}
              further clarifying that represented more generally in the project
              of <strong>Weaponize!</strong>;
              <a name="fnref:9" href="#fn:9" className="fgoto">
                9
              </a>{" "}
              indeed might we detect some semblance of its aesthetic aims
              protruding in the very shape of this convergence. As such might
              this tendency as demonstrated here and hereafter be considered
              definitive of that exhibited by this publication more broadly.
            </em>
          </Paragraph>
          <ul className="text-sm list-decimal px-5 leading-6 mb-2">
            <li id="fn:1">
              <Paragraph>
                https://rhea.art/code-is-law-shall-be-the-whole-of-the-law{" "}
                <a href="#fnref:1" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:2">
              <Paragraph>
                https://www.openculture.com/2017/03/an-animated-introduction-to-noam-chomskys-manufacturing-consent.html{" "}
                <a href="#fnref:2" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:3">
              <Paragraph>
                For an uncommonly apt and sophisticated assessment of this
                juxtaposition of trust vs. trustlessness, see Kei Kreutler’s
                piece on{" "}
                <ExternalLink
                  href="https://technosphere-magazine.hkw.de/p/The-Byzantine-Generalization-Problem-Subtle-Strategy-in-the-Context-of-Blockchain-Governance-8UNNcM8VShTpBGWRuob1GP"
                  colortexthover="rainbow_text_hover"
                >
                  The Byzantine Generalization Problem
                </ExternalLink>
                .{" "}
                <a href="#fnref:3" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:4">
              <Paragraph>
                Referring largely here to{" "}
                <ExternalLink
                  href="https://jacobinmag.com/2021/08/walter-benjamin-critique-of-violence-revolution-working-class-kapp-putsch#ch-3"
                  colortexthover="rainbow_text_hover"
                >
                  Walter Benjamin’s notion of law-making and law-preserving
                  violence
                </ExternalLink>{" "}
                as demonstrating a mythic character or status.{" "}
                <a href="#fnref:4" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:5">
              <Paragraph>
                We’re here using the term in a sense not wholly distinct from{" "}
                <ExternalLink
                  href="https://vitalik.ca/general/2021/03/23/legitimacy.html"
                  colortexthover="rainbow_text_hover"
                >
                  that deployed by Vitalik Buterin
                </ExternalLink>
                .{" "}
                <a href="#fnref:5" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:6">
              <Paragraph>
                https://www.rapidexpedition.org/{" "}
                <a href="#fnref:6" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:7">
              <Paragraph>
                https://medium.com/weaponize/a-note-on-our-subject-matter-eda2da050170{" "}
                <a href="#fnref:7" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:8">
              <Paragraph>
                https://medium.com/weaponize/american-contra/home{" "}
                <a href="#fnref:8" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
            <li id="fn:9">
              <Paragraph>
                https://medium.com/weaponize{" "}
                <a href="#fnref:9" className="freturn">
                  ↩
                </a>
              </Paragraph>
            </li>
          </ul>
          <div className="w-full p-2 my-2 flex items-center space-x-2 justify-center">
            <VictoryHand />
            <p className="text-black dark:text-white uppercase font-bold inline-flex">
              Don't Take it Personal
            </p>
          </div>
        </div>
        <EndPostSection telegramid="7">
          <Comments telegramdiscussurl="ainulchannel/7" />
        </EndPostSection>
      </div>
    </Container>
  );
}
