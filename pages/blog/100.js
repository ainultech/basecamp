import Container from "../../components/Container";
import BlogHeader from "../../components/structures/BlogHeader";
import { NextSeo } from "next-seo";
import TwitterSEO from "../../components/TwitterSEO";
import Paragraph from "../../components/elements/block/Paragraph";
import { IconBrandTwitter, IconWorld } from "@tabler/icons";
import CopyToClipboard from "react-copy-to-clipboard";
import toast from "react-hot-toast";
import EndPostSection from "../../components/structures/EndPostSection";
import Comments from "../../components/Comments";

export default function one_hundred_simple_truths_blog() {
  const seotitle = `100 fakta sederhana`;
  const seodescrip = `100 Fakta Sederhana adalah artikel yang saya posting ulang ke dalam bahasa Indonesia dari 100 Simple Truths oleh @traf - orang yang sangat berbakat yang saya kenal dan dikenal sebagai salah satu pendiri Super.so - alat yang membantu mengubah dengan cepat halaman Notion menjadi sebuah situs. 100 Fakta Sederhana dikompilasi oleh Traf dari banyak sumber dan bagi saya, artikelnya sangat sangat bagus, jadi saya ingin memposting ulang ke dalam bahasa Indonesia.`;
  const seourl = `https://ahmad.ainul.tech/blog/100`;
  const seopreviewimg = "/static/images/100/feature-img.png";
  const headerslug = "100";
  const headerpublish = "2021-08-05";
  const headerreadingtime = "16";
  const headerlanguage = "Indonesian";
  const headerfeatureimggif =
    "https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/l41lLuV3wRmJ0DxCw/giphy.gif";
  const headerauthors = "Ahmad Ainul Rizki";

  function CopyTruthThing() {
    toast(<p className="text-black dark:text-white text-sm">Copied</p>, {
      icon: "👍",
    });
  }

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <BlogHeader
          title={seotitle}
          publishedat={headerpublish}
          readingtime={headerreadingtime}
          language={headerlanguage}
          viewcounterslug={headerslug}
          description={seodescrip}
          featureimggif={headerfeatureimggif}
          authors={headerauthors}
        />
        <div className="text-black dark:text-white space-y-3">
          <Paragraph>
            Memberikan rasa hormat dan hormat yang setinggi-tingginya kepada
            @traf - salah satu Desainer Digital yang telah saya ikuti setiap
            hari untuk waktu yang sangat lama. @traff adalah orang yang memiliki
            cara berpikir yang sangat berbeda dan selalu menciptakan untuk
            dirinya sendiri rasa kagum begitu dia meluncurkan suatu produk.
            @traff terkenal sebagai salah satu pendiri produk yang disebut{" "}
            <a
              href="https://super.so"
              target="_blank"
              rel="noreferrer"
              className="font-bold"
            >
              Super
            </a>{" "}
            - salah satu alatnya bernilai lebih dari $40.000 MMR dalam waktu
            kurang dari setahun sejak debutnya, sebuah angka yang sangat
            berharga layak untuk apa yang diberikannya.
          </Paragraph>
          <Paragraph>
            Kembali ke artikel terbaru @traf 100 Fakta Sederhana, Ini adalah
            artikel yang dia kumpulkan secara pribadi dari berbagai sumber, dari
            pengalaman dan pola pikirnya sendiri. Hampir Saya membacanya setiap
            hari sejak @traf mempostingnya. Itulah alasannya Saya ingin
            memposting ulang di situs saya, tentu saja Saya akan mencoba untuk
            mengulang kata-kata ini dalam bahasa & bahasa Indonesia.
          </Paragraph>
          <Paragraph>
            Postingan aslinya bisa dilihat di:{" "}
            <a
              href="https://tr.af/100"
              target="_blank"
              rel="noreferrer"
              className="font-bold"
            >
              tr.af/100
            </a>
          </Paragraph>
          <Paragraph>Dan ikuti @traff</Paragraph>
          <div>
            <div className="flex items-center space-x-2">
              <a
                href="https://twitter.com/traf"
                target="_blank"
                rel="noreferrer"
              >
                <div className="p-2 inline-flex items-center space-x-1 border-2 border-black dark:border-white">
                  <IconBrandTwitter className="text-black dark:text-white" />
                  <p className="text-black dark:text-white text-sm font-bold">
                    @traf
                  </p>
                </div>
              </a>
              <a href="https://tr.af" target="_blank" rel="noreferrer">
                <div className="p-2 inline-flex items-center space-x-1 border-2 border-black dark:border-white">
                  <IconWorld className="text-black dark:text-white" />
                  <p className="text-black dark:text-white text-sm font-bold">
                    tr.af
                  </p>
                </div>
              </a>
            </div>
          </div>
          <Paragraph>
            Dan inilah dia. Satu catatan penting (terjemahan saya mungkin Agak
            kasar karena saya tidak pandai menerjemahkan), saya pikir terjemahan
            dan bahasa Inggrisnya masih memiliki makna yang sesuai. Kamu juga
            dapat mengklik setiap kalimat untuk menyalin kalimat dengan cepat.
          </Paragraph>
          <div className="pl-9">
            <ul className="list-decimal text-black dark:text-white text-xl font-bold">
              {OneHunredSimpleTruths.map((truth, index) => (
                <li key={index} className="text-black dark:text-white">
                  <CopyToClipboard text={truth[0]} onCopy={CopyTruthThing}>
                    <p className="font-bold text-xl rainbow_text_hover cursor-pointer transition-all active:scale-[0.9]">
                      {truth[0]}
                    </p>
                  </CopyToClipboard>
                  <p className="text-sm font-normal">{truth[1]}</p>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <EndPostSection telegramid="6">
          <Comments telegramdiscussurl="ainulchannel/6" />
        </EndPostSection>
      </div>
    </Container>
  );
}

export const OneHunredSimpleTruths = [
  [
    "It’s 100% off if you don’t buy it.",
    "Ini 100% gratis jika kami tidak membelinya.",
  ],
  [
    "Not wanting something is as good as having it.",
    "Tidak menginginkan sesuatu sama baiknya dengan memilikinya.",
  ],
  [
    "Someone else’s success isn’t your failure.",
    "Keberhasilan orang lain bukanlah kegagalanmu.",
  ],
  [
    "A question opens the mind, a statement keeps it closed.",
    "Sebuah pertanyaan membuka pikiran, sebuah pernyataan membuatnya tertutup.",
  ],
  [
    "Spending time on things you can't control takes time away from things you can.",
    "Menghabiskan waktu untuk hal-hal yang tidak dapat kamu kendalikan membutuhkan waktu lebih dari hal-hal yang dapat kamu kendalikan.",
  ],
  [
    "Being creative is just combining interest with initiative.",
    "Kreativitas hanya menggabungkan minat dengan inisiatif.",
  ],
  [
    "Success, like happiness, cannot be pursued. It must ensue.",
    "Sukses, seperti kebahagiaan, tidak bisa dikejar. Itu akan terjadi kemudian.",
  ],
  [
    "Value compliments and insults the same.",
    "Nilai pujian dan hinaan adalah sama.",
  ],
  [
    "Consuming information won't make you smart, applying it will.",
    "Menggunakan informasi tidak akan membuat kamu pintar, tetapi menerapkannya akan.",
  ],
  [
    "Be happy with what you have, while you work for what you want.",
    "Berbahagialah dengan apa yang kamu miliki, sementara kamu bekerja untuk apa yang kamu inginkan.",
  ],
  [
    "Life is too short to not be pursuing the best opportunity you know of.",
    "Hidup ini terlalu singkat untuk tidak mengejar peluang terbaik yang kamu tahu.",
  ],
  [
    "Inspiration is a productivity multiplier, but it’s perishable. Act quickly.",
    "Inspirasi adalah faktor produktivitas, tetapi dapat dengan mudah hilang. Ambil tindakan dengan cepat.",
  ],
  [
    "Minimalism isn't about owning nothing, it’s about nothing owning you.",
    "Minimalisme bukan tentang tidak memiliki apa-apa, ini tentang tidak ada yang memiliki kamu.",
  ],
  [
    "Demotivated because of how long it’ll take? Remember the time will pass anyways.",
    "Berkurang-nya motivasi karena itu akan memakan waktu lama? Selalu ingat waktu selalu berlalu.",
  ],
  [
    "People don't care what you can do, they care about what you can do for them.",
    "Orang tidak peduli apa yang bisa kamu lakukan, mereka peduli apa yang bisa kamu lakukan untuk mereka.",
  ],
  [
    "How much you eat affects your size, how much you move affects your shape.",
    "Seberapa banyak kamu makan memengaruhi ukuranmu, seberapa banyak kamu bergerak memengaruhi bentuk tubuhmu.",
  ],
  [
    "Not having the time for it is another way of saying it’s not important.",
    "Tidak punya waktu untuk itu adalah cara lain untuk mengatakan hal tersebut tidak penting.",
  ],
  [
    "Choose consistency over intensity, because consistency compounds.",
    "Pilih konsistensi daripada intensitas, karena konsistensi itu majemuk.",
  ],
  [
    "Most traditions are solutions to forgotten problems.",
    "Kebanyakan tradisi adalah solusi untuk masalah yang terlupakan.",
  ],
  [
    "You were born in one day, you will die in one day, you can change in one day.",
    "Kamu lahir dalam satu hari, kamu akan mati dalam satu hari, kamu dapat berubah dalam satu hari.",
  ],
  [
    "Yesterday is gone, tomorrow isn't promised, today is all there is.",
    "Kemarin sudah berlalu, besok tidak pasti, hari ini semuanya ada.",
  ],
  [
    "Focus on nothing when it doesn’t matter, so you’ll focus better when it does.",
    "Jangan fokus pada apa pun ketika itu tidak penting, jadi kamu akan lebih fokus ketika itu penting.",
  ],
  [
    "More choices often lead to less action. Do more by doing less.",
    "Lebih banyak pilihan sering menyebabkan lebih sedikit tindakan. Lakukan lebih banyak dengan melakukan lebih sedikit.",
  ],
  [
    "It’s not the thing itself that upsets you, but your interpretation of it.",
    "Bukan hal itu sendiri yang membuat kamu kesal, tetapi interpretasi kamu tentangnya.",
  ],
  [
    "Focus is repeatedly saying no to almost everything.",
    "Terus fokus untuk mengatakan tidak pada hampir semua hal.",
  ],
  [
    "You can either build your dream, or help someone build theirs.",
    "Kamu dapat membangun impianmu atau membantu seseorang membangun impian mereka.",
  ],
  [
    "Stop trading time for money, start trading value for money.",
    "Saatnya berhenti memperdagangkan waktu untuk uang, mulailah memperdagangkan nilai uang.",
  ],
  [
    "Learning how to learn is one of the best investments of your time.",
    "Mempelajari cara belajar adalah salah satu investasi terbaik dari waktumu.",
  ],
  [
    "Your circumstances have nothing to do with your emotional state.",
    "Keadaan kamu tidak ada hubungannya dengan keadaan emosionalmu.",
  ],
  [
    "Discipline is choosing between what you want now and what you want most.",
    "Disiplin adalah memilih antara apa yang kamu inginkan sekarang dan apa yang paling kamu inginkan.",
  ],
  [
    "The real value of money is what you can afford to no longer do.",
    "Nilai nyata untuk uang adalah apa yang dapat kamu bayar untuk berhenti melakukannya.",
  ],
  [
    "If you lower your expectations, you’ll rarely be disappointed.",
    "Jika kamu menurunkan harapanmu, Kamu akan jarang kecewa.",
  ],
  [
    "Creativity is an infinite resource—the more you spend, the more you have.",
    "Kreativitas adalah sumber daya yang tidak terbatas — semakin banyak yang kamu belanjakan, semakin banyak yang kamu dapatkan.",
  ],
  [
    "Every 'no' improves your ability to fulfill on a future yes.",
    "Setiap 'tidak' akan meningkatkan kemampuan kamu untuk menyelesaikan jawaban 'ya' di masa mendatang.",
  ],
  [
    "The gap between where you are and where you want to be is closer than you think.",
    "Jarak antara tempat kamu berada dan tempat yang kamu inginkan lebih dekat dari yang kamu kira.",
  ],
  [
    "Your ideas are worth nothing without the will to act on them.",
    "Ide-ide kamu tidak berharga tanpa keinginan untuk bertindak.",
  ],
  [
    "Be interesting to others by being interested in them.",
    "Jadilah menarik bagi orang lain dengan menjadi tertarik pada mereka.",
  ],
  [
    "Every single person you meet knows something you don’t.",
    "Setiap orang yang kamu temui tahu sesuatu yang tidak kamu ketahui.",
  ],
  [
    "Speak when you have something to say, not when you want to say something.",
    "Bicaralah ketika kamu memiliki sesuatu untuk dikatakan, bukan ketika kamu ingin mengatakan sesuatu.",
  ],
  [
    "Mediocrity is the path of least resistance, also making it the most competitive.",
    "Biasa-biasa saja adalah jalan yang paling sedikit perlawanannya, dan juga yang paling kompetitif.",
  ],
  [
    "Being busy is not a badge of honor—it's a lack of freedom.",
    "Sibuk bukanlah lencana kehormatan — itu adalah kurangnya kebebasan.",
  ],
  [
    "Your thoughts and feelings are choices. Control them or they'll control you.",
    "Pikiran dan perasaan kamu adalah pilihan. Kendalikan mereka atau mereka akan mengendalikanmu.",
  ],
  [
    "You don't lack motivation, you lack a better reason.",
    "Kamu tidak kekurangan motivasi, kamu hanya kekurangan alasan yang lebih baik.",
  ],
  [
    "Time is your most valuable resource, spend it wisely.",
    "Waktu adalah sumber daya kamu yang paling berharga, gunakan dengan bijak.",
  ],
  [
    "Admit when you’re wrong, show humility when you’re right.",
    "Akui ketika kamu salah, tunjukkan kerendahan hati ketika kamu benar.",
  ],
  [
    "Your brain doesn’t fill up with more information, it expands along with it.",
    "Otak kamu tidak terisi dengan lebih banyak informasi, ia berkembang seiring dengan itu.",
  ],
  [
    "If it takes less than 5 minutes, don’t schedule it, do it now.",
    "Jika kurang dari 5 menit, jangan jadwalkan, lakukan sekarang.",
  ],
  [
    "Your biggest competitor in life is yourself.",
    "Saingan terbesarmu dalam hidup adalah diri kamu sendiri.",
  ],
  [
    "Life only gives you what you decided you could have.",
    "Hidup hanya memberimu apa yang kamu putuskan untuk kamu miliki.",
  ],
  [
    "The internet removes the barrier from how much you can learn about anything.",
    "Internet menghilangkan penghalang kamu untuk mempelajari tentang apa pun.",
  ],
  [
    "Ignore the noise, people will criticize you no matter what you do.",
    "Abaikan kebisingannya, orang akan mengkritikmu apa pun yang kamu lakukan.",
  ],
  [
    "Avoiding stupidity can often be a better strategy than seeking out brilliance.",
    "Menghindari kebodohan seringkali bisa menjadi strategi yang lebih baik daripada mencari kecerdasan.",
  ],
  [
    "If you have time to consume, you have time to create.",
    "Jika kamu punya waktu untuk mengkonsumsi sesuatu, kamu juga punya waktu untuk menciptakan sesuatu.",
  ],
  [
    "There’s no right or wrong time, there’s just time and what you choose to do with it.",
    "Tidak ada waktu yang benar atau salah, yang ada hanyalah waktu dan apa yang kamu pilih untuk dilakukan dengannya.",
  ],
  [
    "You can steal all the ideas you want, but action can only come from within.",
    "Kamu dapat mencuri semua ide yang kamu inginkan, tetapi tindakan hanya bisa datang dari dalam.",
  ],
  [
    "Get comfortable with changing your mind after learning new information.",
    "Jangan ragu untuk berubah pikiran setelah mempelajari informasi baru.",
  ],
  [
    "Food fuels your brain, information fuels your mind. You are what you consume.",
    "Makanan bahan bakar otakmu, informasi bahan bakar pikiranmu. Kamu adalah apa yang kamu konsumsi.",
  ],
  [
    "Read to find new ideas, write to understand them, implement to learn from them.",
    "Membaca untuk menemukan ide-ide baru, menulis untuk memahaminya, menerapkan untuk belajar darinya.",
  ],
  [
    "Your current habits are a sneak peek of your desired future.",
    "Kebiasaan kamu saat ini adalah gambaran masa depan yang kamu inginkan.",
  ],
  [
    "The best teachers are your previous mistakes.",
    "Guru terbaik adalah kesalahan kamu sebelumnya.",
  ],
  [
    "Working hard doesn’t get you anywhere if you’re working on the wrong thing.",
    "Bekerja keras tidak akan membawamu kemana-mana jika kamu mengerjakan hal yang salah.",
  ],
  [
    "Being alone is physical, feeling alone is mental.",
    "Sendirian itu fisik, merasa kesepian itu spiritual.",
  ],
  [
    "Silence is a blank canvas for your thoughts.",
    "Diam adalah kanvas kosong untuk pikiranmu.",
  ],
  [
    "If it doesn't feel like work, no one can compete with you.",
    "Jika tidak terasa seperti pekerjaan, tidak ada yang bisa menandingimu.",
  ],
  [
    "Compare upwards and feel envy, compare downwards and feel grateful.",
    "So sánh phía trên và cảm thấy ghen tị, so sánh phía dưới và cảm thấy biết ơn.",
  ],
  [
    "You are not what you say or think, you are what you do.",
    "Bandingkan ke atas dan merasa iri, bandingkan ke bawah dan merasa bersyukur.",
  ],
  [
    "There are no million dollar ideas, only million dollar executions.",
    "Tidak ada ide jutaan dolar, hanya implementasi jutaan dolar.",
  ],
  [
    "What you believe is what you settle for.",
    "Apa yang kamu yakini adalah apa yang kamu setujui.",
  ],
  [
    "Saying yes benefits others, saying no benefits you.",
    "Mengatakan ya menguntungkan orang lain, mengatakan tidak menguntungkan kamu.",
  ],
  [
    "The quality of your mind determines the quality of your life.",
    "Kualitas pikiran kamu menentukan kualitas hidupmu.",
  ],
  [
    "Being great is just being consistently good.",
    "Menjadi hebat adalah menjadi baik secara konsisten.",
  ],
  [
    "Spending time on things that buy you time is always a good use of it.",
    "Meluangkan waktu untuk hal-hal yang memberi kamu waktu selalu merupakan penggunaan yang baik.",
  ],
  [
    "The dots only connect in retrospect.",
    "Titik-titik hanya terhubung dalam retrospeksi.",
  ],
  [
    "Consuming puts you to work for the internet, creating puts the internet to work for you.",
    "Mengkonsumsi membuat kamu bekerja untuk internet, mencipta membuat internet bekerja untuk kamu.",
  ],
  [
    "Hours don't equal output.",
    "Jumlah jam kamu bekerja tidak berarti jumlah output yang kamu hasilkan sama.",
  ],
  [
    "Luck favors those in motion.",
    "Keberuntungan berpihak pada mereka yang bergerak.",
  ],
  [
    "What separates creators & consumers isn't the ability to create, but the willingness to publish.",
    "Yang membedakan pencipta dari konsumen bukanlah kreativitas tetapi kemauan untuk mempublikasikan.",
  ],
  [
    "If you never get bored of learning, you'll never get tired of living.",
    "Jika kamu tidak pernah bosan belajar, kamu tidak akan pernah bosan hidup.",
  ],
  [
    "The more you move, the easier it is to keep moving.",
    "Semakin banyak kamu bergerak, semakin mudah untuk terus bergerak.",
  ],
  [
    "The only productivity hack you need is enjoying what you do.",
    "Satu-satunya peningkatan produktivitas yang kamu butuhkan adalah menikmati apa yang kamu lakukan.",
  ],
  [
    "How you do anything, is how you do everything.",
    "Apa pun yang kamu lakukan, adalah bagaimana kamu melakukan sesuatu.",
  ],
  [
    "Beliefs are more about belonging than they are about truth.",
    "'Keyakinan' lebih tentang kepemilikan daripada tentang kebenaran.",
  ],
  [
    "Continuous improvement is better than delayed perfection.",
    "Perbaikan terus-menerus lebih baik daripada penyelesaian yang tertunda.",
  ],
  [
    "Do many things that you like to find the few things that you love.",
    "Lakukan hal-hal yang kamu sukai untuk menemukan hal-hal yang kamu sukai.",
  ],
  [
    "Your first impression isn’t your appearance, it’s your energy.",
    "Kesan pertama kamu bukanlah penampilanmu, tetapi energi kamu.",
  ],
  [
    "You're not bad at something, you're just new to it.",
    "Kamu tidak buruk dalam sesuatu, kamu hanya pemula dalam hal itu.",
  ],
  [
    "Death is the price we pay for life, learn to make peace with it.",
    "Kematian adalah harga yang harus kita bayar untuk hidup, belajarlah berdamai dengannya.",
  ],
  [
    "The sooner you pay the price, the less it will cost you in the long run.",
    "Semakin cepat kamu membayar harganya, semakin sedikit biaya yang akan kamu keluarkan dalam jangka panjang.",
  ],
  [
    "Motivation more often comes after starting, not before.",
    "Motivasi biasanya datang setelah memulai, bukan sebelumnya.",
  ],
  [
    "Integrity is being the same person no matter the circumstance.",
    "Integritas adalah menjadi orang yang sama dalam keadaan apapun.",
  ],
  [
    "Increase your rate of success by increasing your rate of failure.",
    "Tingkatkan tingkat keberhasilan kamu dengan meningkatkan tingkat kegagalanmu.",
  ],
  [
    "The cost of anything in life is either money, time, or attention.",
    "Biaya apa pun dalam hidup adalah uang, waktu, atau perhatian.",
  ],
  [
    "Discipline is doing what you hate to do as if you love it.",
    "Disiplin adalah melakukan apa yang kamu benci untuk dilakukan seolah-olah kamu menyukainya.",
  ],
  [
    "The less you have to work with, the more you'll do with what you have.",
    "Semakin sedikit kamu harus bekerja, semakin banyak yang akan kamu lakukan dengan apa yang kamu miliki.",
  ],
  [
    "Absorb what's useful, discard what isn't, add what's uniquely yours.",
    "Serap apa yang berguna, buang apa yang tidak, tambahkan apa yang unik milikmu.",
  ],
  [
    "Most bad things in life come from either too much or too little of any one thing.",
    "Sebagian besar hal buruk dalam hidup berasal dari terlalu banyak atau terlalu sedikit dari satu hal.",
  ],
  [
    "If you’re reading this, you have everything you need to start.",
    "Jika kamu membaca ini, kamu memiliki semua yang kamu butuhkan untuk memulai.",
  ],
  [
    "Peace is the absence of desire.",
    "Kedamaian adalah tidak adanya keinginan.",
  ],
  ["No one owes you anything.", "Tidak ada yang berutang apa pun kepadamu."],
  ["See #99.", "Baca kembali Pasal #99"],
];
