import { NextSeo } from "next-seo";
import Container from "../components/Container";
import { Client } from "@notionhq/client";
import NextLink from "next/link";
import { ArrowUpRight, FaceHappy, FaceVeryHappy } from "akar-icons";
import useTranslate from "next-translate/useTranslation";
import { useRouter } from "next/router";
import TwitterSEO from "../components/TwitterSEO";

export async function getStaticProps() {
  const notion = new Client({ auth: process.env.NOTION_API_OFFICIAL_KEYS });

  const databaseId = process.env.NOTION_PAGE_ID_TOOLS_PAGE;
  const response = await notion.databases.query({
    database_id: databaseId,
  });
  return {
    props: {
      results: response.results,
    },
  };
}

export default function Tools({ results }) {
  const router = useRouter();
  const { t } = useTranslate("tools");
  const seotitle = `Tools - Ainul.tech`;
  const seodescrip = `Tools is a page that aggregates applications / libraries / websites / tools that Ainul feels are quite useful for work and daily life. Compiled from many sources on the Internet.`;
  const seourl = `https://ahmad.ainul.tech/tools`;
  const seopreviewimg = "/static/tools-feature-img.png";

  const getPageDisplay_Important = () => {
    let jsx = [];

    results
      .filter((block) =>
        block.properties.tags.multi_select
          .map((sel) => sel.name)
          .includes("Important")
      )
      .forEach((block) => {
        jsx.push(
          <div
            className="text-black dark:text-white border-black dark:border-white border-2 flex items-center"
            key={block.id}
          >
            <div className="w-[20%] md:w-[10%] flex items-center justify-center bg-white h-full">
              <div className="w-7 h-7 relative">
                <img
                  src={`https://ik.imagekit.io/ainul/bridge/${block.properties.icon.files[0].name}`}
                  className="w-full object-cover absolute"
                />
              </div>
            </div>
            <div className="w-[70%] py-2 px-4 border-l-2 border-r-2 border-black dark:border-white">
              <h3 className="font-sourcecodepro uppercase text-base">
                {block.properties.name.title[0].plain_text}
              </h3>
              <p className="text-sm">
                {router.locale === "id-ID"
                  ? block.properties.idabout.rich_text[0].plain_text
                  : block.properties.enabout.rich_text[0].plain_text}
              </p>
            </div>
            <div className="w-[30%] md:w-[20%] flex items-center justify-center">
              <a
                href={block.properties.url.url}
                target="_blank"
                rel="noreferrer"
                className="font-sourcecodepro text-base uppercase flex items-center space-x-1"
              >
                <p className="inline-flex text-lg">{t("tools-notion-visit")}</p>
                <ArrowUpRight
                  className="text-black dark:text-white"
                  size={22}
                />
              </a>
            </div>
          </div>
        );
      });
    return jsx;
  };

  const getPageDisplay_Support = () => {
    let jsx = [];

    results
      .filter((block) =>
        block.properties.tags.multi_select
          .map((sel) => sel.name)
          .includes("Support")
      )
      .forEach((block) => {
        jsx.push(
          <div
            className="text-black dark:text-white border-black dark:border-white border-2 flex items-center"
            key={block.id}
          >
            <div className="w-[20%] md:w-[10%] flex items-center justify-center bg-white h-full">
              <div className="w-7 h-7 relative">
                <img
                  src={`https://ik.imagekit.io/ainul/bridge/${block.properties.icon.files[0].name}`}
                  className="w-full object-cover absolute"
                />
              </div>
            </div>
            <div className="w-[70%] py-2 px-4 border-l-2 border-r-2 border-black dark:border-white">
              <h3 className="font-sourcecodepro uppercase text-base">
                {block.properties.name.title[0].plain_text}
              </h3>
              <p className="text-sm">
                {router.locale === "id-ID"
                  ? block.properties.idabout.rich_text[0].plain_text
                  : block.properties.enabout.rich_text[0].plain_text}
              </p>
            </div>
            <div className="w-[30%] md:w-[20%] flex items-center justify-center">
              <a
                href={block.properties.url.url}
                target="_blank"
                rel="noreferrer"
                className="font-sourcecodepro text-base uppercase flex items-center space-x-1"
              >
                <p className="inline-flex text-lg">{t("tools-notion-visit")}</p>
                <ArrowUpRight
                  className="text-black dark:text-white"
                  size={22}
                />
              </a>
            </div>
          </div>
        );
      });
    return jsx;
  };

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4 pt-5">
        <div className="inline-flex items-center space-x-1 text-black dark:text-white text-base uppercase font-sourcecodepro">
          <NextLink href="/">
            <a>
              <p className="text-gray-300 dark:text-gray-600 hover:text-black dark:hover:text-white">
                FRONT-PAGE
              </p>
            </a>
          </NextLink>
          <p>/</p>
          <h1>Tools</h1>
        </div>
      </div>
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <div className="inline-flex space-x-2 md:space-x-4 bg-gray-100 dark:bg-gray-800 p-2 rounded-md items-start md:items-center">
          <a href="https://notion.so" target="_blank" rel="noreferrer">
            <img src="/icons/notion.svg" className="w-7 h-7 dark:fill-white" />
          </a>
          <p className="text-black dark:text-white uppercase text-sm">
            {t("tools-notion-p1")}{" "}
            <a
              href="https://developers.notion.com/"
              target="_blank"
              rel="noreferrer"
              className="rainbow_text_hover font-bold"
            >
              Notion Official API
            </a>{" "}
            <br />
            {t("tools-notion-p2a")}{" "}
            <a
              href="https://ainul.notion.site/8e87e48f7d10411d9db7c06935403d4c?v=272f5ff51a1f4a9d8118f89e1b022e2b"
              target="_blank"
              rel="noreferrer"
              className="rainbow_text_hover font-bold"
            >
              {t("tools-notion-p2b")}
            </a>
          </p>
        </div>
        <div>
          <div className="flex items-center space-x-2">
            <FaceVeryHappy className="text-black dark:text-white" size={22} />
            <h2 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
              Important
            </h2>
          </div>
          <div className="my-4 grid grid-cols-1 gap-4">
            {getPageDisplay_Important()}
          </div>
          <div className="flex items-center space-x-2 pt-4">
            <FaceHappy className="text-black dark:text-white" size={22} />
            <h2 className="text-black dark:text-white font-sourcecodepro uppercase text-base">
              Support
            </h2>
          </div>
          <div className="my-4 grid grid-cols-1 gap-4">
            {getPageDisplay_Support()}
          </div>
        </div>
      </div>
    </Container>
  );
}
