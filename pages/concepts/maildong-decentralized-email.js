import Container from "../../components/Container";
import { NextSeo } from "next-seo";
import TwitterSEO from "../../components/TwitterSEO";
import {
  Air,
  ArrowUpRight,
  Clipboard,
  CloudDownload,
  Edit,
  File,
  Inbox,
  Pin,
  Telescope,
  VictoryHand,
} from "akar-icons";
import dodont from "@ainultech/dodont";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { twilight } from "react-syntax-highlighter/dist/cjs/styles/prism";
import ExternalLink from "../../components/elements/ExternalLink";
import Paragraph from "../../components/elements/block/Paragraph";
import Heading2 from "../../components/elements/block/Heading2";

export default function MaildongDecentralizedEmail() {
  const seotitle = `Maildong - Decentralized Email`;
  const seodescrip = `An SMTP-compatible "mail" client that uses Ceramic documents to sync & relay email-like messages between identities. Roughly related to did-comm.`;
  const seourl = `https://ahmad.ainul.tech/concepts/maildong-decentralized-email`;
  const seopreviewimg =
    "/static/images/concepts/maildong-decentralized-email-feature-img.png";

  return (
    <Container>
      <NextSeo
        title={seotitle}
        description={seodescrip}
        canonical={seourl}
        openGraph={{
          seotitle,
          seodescrip,
          seourl,
          images: [
            {
              url: seopreviewimg,
              alt: seotitle,
              width: 1200,
              height: 628,
            },
          ],
        }}
      />
      <TwitterSEO
        currentURL={seourl}
        previewImage={`https://ahmad.ainul.tech/${seopreviewimg}`}
        siteName={seotitle}
        pageTitle={seotitle}
        description={seodescrip}
      />
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <div className="space-y-2 border_dash_animated dark:border_dash_animated_dark">
          <div>
            <h1 className="text-xl font-bold uppercase font-sourcecodepro text-black dark:text-white">
              'Maildong - Decentralized Email'
            </h1>
            <p className="text-sm text-black dark:text-white">
              An SMTP-compatible "mail" client that uses Ceramic documents to
              sync & relay email-like messages between identities. Roughly
              related to did-comm.
            </p>
          </div>
          <div className="text-black dark:text-white grid grid-cols-1 md:grid-cols-2 gap-1">
            <div className="">
              <a href="#" target="_blank" rel="noreferrer">
                <div className="flex py-1 items-center text-sm group">
                  <CloudDownload className="mr-1" size={22} />
                  Download PDF (available when done)
                  <ArrowUpRight
                    className="mr-1 opacity-0 group-hover:opacity-100"
                    size={22}
                  />
                </div>
              </a>
              <a
                href="https://www.notion.so/isnhp/Sound-from-Universe-580bc4fad2dd48e882fe3bff6e16f840"
                target="_blank"
                rel="noreferrer"
              >
                <div className="flex py-1 items-center text-sm group">
                  <Inbox className="mr-1" size={22} />
                  Read on Notion
                  <ArrowUpRight
                    className="mr-1 opacity-0 group-hover:opacity-100"
                    size={22}
                  />
                </div>
              </a>
              <a
                href="https://gitlab.com/ainultech/basecamp/-/blob/main/pages/concepts/maildong-decentralized-email.js"
                target="_blank"
                rel="noreferrer"
              >
                <div className="flex py-1 items-center text-sm group">
                  <Edit className="mr-1" size={22} />
                  Edit on Gitlab
                  <ArrowUpRight
                    className="mr-1 opacity-0 group-hover:opacity-100"
                    size={22}
                  />
                </div>
              </a>
              <div className="flex py-1 items-center text-sm">
                <Air className="mr-1" size={22} />
                Status: Writing
              </div>
            </div>
            <div className="space-y-2">
              <div className="flex items-center text-sm">
                <Pin className="mr-1" size={22} />
                Published at: 2021-09-01
              </div>
              <div className="flex items-center text-sm">
                <File className="mr-1" size={22} />
                Written by: Ahmad Ainul Rizki
              </div>
              <div className="flex items-center text-sm">
                <Telescope className="mr-1" size={22} />
                Experiment by: Ahmad Ainul Rizki
              </div>
              <div className="flex items-center text-sm">
                <Clipboard className="mr-1" size={22} />
                Review by: ...
              </div>
            </div>
          </div>
        </div>

        <div className="text-black dark:text-white">
          <Heading2>Some Fundamentals</Heading2>
          <Paragraph>
            The fundamental idea of a self-sovereign identity is the "DID", a
            decentralized identifier, looking like{" "}
            <code>did:key:zkmanycharacters</code>. DIDs are uniquely identified
            by the key material of a user, derived by some random seed only the
            user knows. DIDs can be resolved/expanded to DID documents that
            contain information about the public keys a user uses for
            communication and authentication purposes.
          </Paragraph>
          <Paragraph>
            At its core, the Ceramic protocol uses DIDs to represent identities
            that are interacting with documents, atm did:3 and did:key are
            supported:{" "}
            <ExternalLink
              href="https://developers.ceramic.network/learn/advanced/overview/#authentication"
              colortexthover="rainbow_text_hover"
            >
              https://developers.ceramic.network/learn/advanced/overview/#authentication
            </ExternalLink>
            . On top of Ceramic, IDX is adding a layer of well-formed documents
            that contain additional information about the user, e.g. a user
            "profile".
          </Paragraph>
          <Paragraph>
            Ceramic is a "smart" document protocol that allows users to
            collaboratively document contents. A document is the result of
            applying many CRDT based changesets in the right order. It's the job
            of Ceramic nodes and the Ceramic network (Clay) to keep track of the
            changeset ordering and anchoring them on arbitrary blockchains /
            pinning their content on IPFS. Ceramic nodes make use of a
            cryptographically modified DAG component in IPFS (dag-jose) to
            create DID-crypto compatible IPLD relations between changesets. This
            all leads to one fundamental concept: each document gets one unique
            key after its creation ("genesis doc") and is modified by a sequence
            of change logs applied to it. Since a ceramic client enforces
            documents to adhere to predefined schemas and definitions, document
            types can contain logic that's applied when users are making changes
            to / reading those documents. Hence they call them "smart".
          </Paragraph>
          <Heading2>
            An "email" like messaging protocol on top of Ceramic
          </Heading2>
          <Paragraph>First, without "SMTP" integration.</Paragraph>
          <Paragraph>
            A user runs a local "mail" client that's nothing but a client
            connected to a ceramic node (thereby connected to IPFS & blockchains
            which are hidden technical details of the ceramic protocol).
          </Paragraph>
          <Paragraph>
            To interact with a ceramic node, a ceramic client must identify
            itself with a DID (see client initialization code).
          </Paragraph>
          <Paragraph>
            A user can create a mail message, containing recipient subject
            payload
          </Paragraph>
          <Paragraph>
            These fields are wrapped into a new dispatchable ceramic document
            that's controlled by the sending party, roughly using these
            schematics:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            recipient: did:key:...
            payload: __encrypted by (did:key:sender#encryption-key):__
              sender: did:key:sender...
              recipient: did:key...
              subject:...
              payload:...
            __/encrypted__
          `}
          </SyntaxHighlighter>
          <Paragraph>
            The recipient can be any DID that's resolvable by the ceramic
            protocol (did:3 / did:key)
          </Paragraph>
          <Paragraph>
            Upon sending I'm encrypting the message's content with the
            recipients' public encryption key (think this can be safely done
            inside ceramic already).
          </Paragraph>
          <Paragraph>
            By adding the "message" document to ceramic it gets a unique ceramic
            document id like <code>ceramic://bafyfsdifsduifh</code>.
          </Paragraph>
          <Heading2>Relaying</Heading2>
          <Paragraph>
            Every user / DID create a relay document that's world writeable (or
            writeable by DIDs the user trusts but that's another story.) that
            uses a schema like:
          </Paragraph>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            {inbox: [Message]}
            ...

            Every user is *listening* locally on his relay document for updates.

            To "send" a mail to another user, the sender updates the recipient's relay document by adding his message document **id** to its inbox field, e.g.:

          `}
          </SyntaxHighlighter>
          <SyntaxHighlighter
            showLineNumbers={false}
            showInlineLineNumbers={false}
            wrapLongLines={false}
            style={twilight}
            customStyle={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {dodont`
            { inbox: [ "ceramic://bafysomeidofanotherguy", "ceramic://bafythemessageiwanttosend", //<-- ... ] }
          `}
          </SyntaxHighlighter>
          <Paragraph>
            Since the recipient gets a notification about changes on his relay
            document he can fetch the content of{" "}
            <code>ceramic://bafythemessageiwanttosend</code> using the ceramic
            protocol on his machine, decrypt the payload with his private key
            and store the decrypted message locally (not necessary, it's only
            mandatory to list them)
          </Paragraph>
          <Heading2>Nice to Haves</Heading2>
          <Paragraph>
            If the relay documents are publicly writeable, every user might
            delete the entry for every other user. That's not good ;) A custom
            Ceramic DocType should be perfectly able to allow only the creator
            (i.e. the owner) to remove entries and every other user to add
            entries.
          </Paragraph>
          <Paragraph>
            To avoid "spam", a user might set controllers for his relay document
            (controllers are DIDs) so only Identities known to the user might
            send him mails.
          </Paragraph>
          <Paragraph>
            (Extension 1, totally unexplained here: all users who want to send
            "blind" emails must add some token stake that the recipient will pay
            back after marking the message as "not spam")
          </Paragraph>
          <Heading2>ENS / Nice names</Heading2>
          <Paragraph>
            Of course, it'll be much nicer if we could not only send emails to a
            DID (didkeyzk21323zwuhsef) but rather using a "readable" address:{" "}
            <ExternalLink
              href="mailto:elamariachi@cemail.eth"
              colortexthover="rainbow_text_hover"
            >
              elamariachi@cemail.eth
            </ExternalLink>
            . We can use ENS to make that happen!
          </Paragraph>
          <Paragraph>
            Someone (we) registers cemail.eth. We set 1 TXT record that contains
            a ceramic document id that represents the global email address
            registry. It's world writeable and lets each IDX DID define a name
            they want to be aliased with (did:keyzk98ur89289 ||
            ceramic://bafyanidxdocumentindex => elmariachi).
          </Paragraph>
          <Paragraph>
            A sender can use that email address in his client.
          </Paragraph>
          <Paragraph>
            Our dispatching backend first resolves the ceramic registry document
            and then looks up a the IDX document id / DID of the recipient.
          </Paragraph>
          <Paragraph>
            (Alternative, maybe simpler: each user registers an "ENS root" we
            can send messages to, that resolves e.g. *@elmariachi.eth)
          </Paragraph>
          <Heading2>SMTP gateway</Heading2>
          <Paragraph>
            Ideally, this whole thing (sending side!) works without a dedicated
            "sending" frontend. Instead, we're using commonly used desktop SMTP
            clients like "Outlook" (find the simplest option here for demo).
          </Paragraph>
          <Paragraph>
            Những điểm hại mà Wishful Thinking có thể gây tác động đến chúng ta
            mà có thể được liệt kê dựa trên những trải nghiệm bản thân và những
            hậu quả mà nó mang lại có thể kể đến
          </Paragraph>
          <Paragraph>
            Our account is "didkeyzkourdidkey", respectively "
            <ExternalLink
              href="mailto:something@our.eth"
              colortexthover="rainbow_text_hover"
            >
              something@our.eth
            </ExternalLink>
            " / "
            <ExternalLink
              href="mailto:elmariachi@cemail.eth"
              colortexthover="rainbow_text_hover"
            >
              elmariachi@cemail.eth
            </ExternalLink>
            " when using ENS aliases
          </Paragraph>
          <Paragraph>
            We don't need a password / any password will do / the private key's
            passphrase (we're not using passwords here)
          </Paragraph>
          <Paragraph>
            Each user is running a small "SMTP" server on his machine. That one
            is configured with the private key (derived by a seed that's
            "safely" stored on disk) for the ceramic identity that matches the
            account name.
          </Paragraph>
          <Paragraph>
            To relay mails for that account, the mail client is using this
            "SMTP" server.
          </Paragraph>
          <Paragraph>
            The server behaves like an SMTP server but under the hood, it'll
            create/relay messages as written above
          </Paragraph>
          <Heading2>Reading frontend</Heading2>
          <Paragraph>
            There's a simple (React) frontend that users can use to fetch/read
            messages. Of course, this could be built with a POP/IMAP interface
            but that will likely lead too far.
          </Paragraph>
          <Paragraph>
            It's just displaying a chronological message list on the left & a
            reading pane on the right.
          </Paragraph>
          <Heading2>Slight UX addons</Heading2>
          <Paragraph>
            Recipients can mark a mail as "read". To do so, they're modifying
            the "read" status of the message document. To be able to do so, a
            sender (or the ceramic doctype) must allow the recipient to change
            the message doc content (esp. the "read" field).
          </Paragraph>
          <div className="w-full p-2 my-2 flex items-center space-x-2 justify-center">
            <VictoryHand />
            <p className="text-black dark:text-white uppercase font-bold inline-flex">
              More soon
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
}
