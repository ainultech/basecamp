import Container from "../components/Container";
import NextLink from "next/link";
import useTranslate from "next-translate/useTranslation";

export default function License() {
  const { t } = useTranslate("license");
  return (
    <Container>
      <div className="items-start mx-auto w-full max-w-3xl">
        <div className="inline-flex items-center space-x-1 text-black dark:text-white text-base uppercase font-sourcecodepro mt-5">
          <NextLink href="/">
            <a>
              <p className="text-gray-300 dark:text-gray-600 hover:text-black dark:hover:text-white">
                FRONT-PAGE
              </p>
            </a>
          </NextLink>
          <p>/</p>
          <h1>License</h1>
        </div>
        <div className="text-black dark:text-white space-y-2 mt-4">
          <h2 className="font-bold text-black dark:text-white text-2xl">
            P License 2021
          </h2>
          <p className="text-black dark:text-white text-sm">
            {t("first-letter")}
          </p>
          <h3 className="text-black dark:text-white font-bold">
            {t("second-letter")}
          </h3>
          <ul className="text-sm space-y-2 list-disc pl-5">
            <li>
              <p>{t("second-letter-list-one")}</p>
            </li>
            <li>
              <p>{t("second-letter-list-two")}</p>
            </li>
            <li>
              <p>{t("second-letter-list-three")}</p>
            </li>
            <li>
              <p>{t("second-letter-list-four")}</p>
            </li>
          </ul>
          <h3 className="text-black dark:text-white font-bold">
            {t("third-letter")}
          </h3>
          <ul className="text-sm space-y-2 list-disc pl-5">
            <li>
              <p>{t("third-letter-list-one")}</p>
            </li>
            <li>
              <p>{t("third-letter-list-two")}</p>
            </li>
            <li>
              <p>{t("third-letter-list-three")}</p>
            </li>
          </ul>
          <h3 className="text-black dark:text-white font-bold">
            {t("fourth-letter")}
          </h3>
          <ul className="text-sm space-y-2 list-disc pl-5">
            <li>
              <p>{t("fourth-letter-list-one")}</p>
            </li>
            <li>
              <p>{t("fourth-letter-list-two")}</p>
            </li>
          </ul>
          <p className="text-black dark:text-white text-sm">
            <p>
              {t("fifth-letter")}
              <strong>ahmad@ainul.tech</strong>.
            </p>
          </p>
        </div>
      </div>
    </Container>
  );
}
