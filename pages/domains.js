import Container from "../components/Container";
import NextLink from "next/link";
import LetterByAinul from "../components/elements/LetterByAinul";
import useTranslate from "next-translate/useTranslation";
import DomainsElement from "../components/elements/DomainsElement";
import { Running, Active, Hodl } from "../components/elements/StatusBadge";

export default function Domains() {
  const { t } = useTranslate("domains");
  return (
    <Container>
      <div className="items-start mx-auto w-full max-w-3xl space-y-4">
        <div className="inline-flex items-center space-x-1 text-black dark:text-white text-base uppercase font-sourcecodepro mt-5">
          <NextLink href="/">
            <a>
              <p className="text-gray-300 dark:text-gray-600 hover:text-black dark:hover:text-white">
                FRONT-PAGE
              </p>
            </a>
          </NextLink>
          <p>/</p>
          <h1>Domains</h1>
        </div>
        <div className="text-black dark:text-white space-y-20">
          <LetterByAinul letterNumber="#0004">
            <p>{t("domains-letter")}</p>
          </LetterByAinul>
          <div className="space-y-4">
            <DomainsElement
              domain="ainul.tech"
              registeredday="20210626"
              visiturl="https://ainul.tech"
              qrcodeimg="/static/domains-ainul-tech-qr-code.png"
            >
              <Running />
            </DomainsElement>
            <DomainsElement
              domain="indiealistic.studio"
              registeredday="20210216"
              visiturl="https://indiealistic.studio"
              qrcodeimg="/static/domains-indiealistic-studio-qr-code.png"
            >
              <Running />
            </DomainsElement>
            <DomainsElement
              domain="kongkowitpku.tech"
              registeredday="20210716"
              visiturl="https://kongkowitpku.tech"
              qrcodeimg="/static/domains-kongkowitpku-tech-qr-code.png"
            >
              <Running />
            </DomainsElement>
            <DomainsElement
              domain="indieland.show"
              registeredday="20210401"
              visiturl="https://indieland.show"
              qrcodeimg="/static/domains-indieland-show-qr-code.png"
            >
              <Active />
            </DomainsElement>
            <DomainsElement
              domain="bejolistic.com"
              registeredday="20190823"
              visiturl="https://bejolistic.com"
              qrcodeimg="/static/domains-bejolistic-com-qr-code.png"
            >
              <Hodl />
            </DomainsElement>
          </div>
        </div>
      </div>
    </Container>
  );
}
