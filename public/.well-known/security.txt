-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Contact: https://ahmad.ainul.tech/human

# If you'd like to encrypt your message, please do so within the the body of the message.
# Our email system doesn't handle PGP-MIME well.
Contact: mailto:security@ainul.tech

Preferred-Languages: en

Encryption: https://ahmad.ainul.tech/pgp/pubkey.txt
Canonical: https://ahmad.ainul.tech/.well-known/security.txt

Expires: Tue, 15 Mar 2022 13:43:01 -0700

-----BEGIN PGP SIGNATURE-----

wsFcBAABCgAGBQJhKzBCAAoJEEe/cZ0HX9jNA7EP/RJ3XtAETXQuZb7zcR8WQ9P3
6vAMjq5c8O9yEim4KJSHUXfd7mWXDlxdxrkT5XihGr3s78PGGfYPljSLYqrOaGVl
JzNs18ROUWxjLp6hqMULBdLfwGXVrZ9MXAXhg1tau+WQ8d+Oqx6hNFCGdAq6d2C7
BKvv2Fwp5K1X0tEzRbEA+WP6DLVIwnvagctTp2uE3mcN1l6WXCWryWIavO6G7XfX
TsS2GnR2KnC/9w/fqOA+YKFkv3jscj8gHoDNk+bx6Un/DHNS8/XfhZ+PxBPMXvnu
O2JkCV4JO9uJeYrNctdiLMlFEA8mQOibdJTuwehwFqww8poZBFl6Y5yT4+WqQC2h
Txn8EKRXOtJkeeH0Bqcy+vkgGnky/PuBdX+G8uoB8yk35JkeeiVqWOp08dy9Wynz
GpJgX6rp1ProN+zLdnEy3hyj8cSlIGmelR5f3lwOR4mBPWxiFucD7j8goE89Mvnk
OjBJj1E37ypbedPNyD39KX/zvJ2s0PuJyCeF4SigTyyz90WK7PgbJn0QJltkUW/T
vViG1cFFHNjEqFz0fLKhfQ9BPgqc9tQReYabmcG3wVupBMAwfNRqoRtg0Eb+pbmY
fgh+VmhuM1ZDQG70suFNlqC45Y+Z/qZNfz3rzzQsKjCHfNvs7RGpOOiHLnR+LmaZ
ncFlACGbM/iRseSdVoIW
=4fnP
-----END PGP SIGNATURE-----
