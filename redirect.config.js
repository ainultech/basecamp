const redirectConfig = [
  {
    source: "/bio",
    destination: "https://bio.link/ainul",
    permanent: false,
  },
  {
    source: "/api",
    destination: "/",
    permanent: false,
  },
  {
    source: "/blog",
    destination: "/",
    permanent: false,
  },
  {
    source: "/concepts",
    destination: "/",
    permanent: false,
  },
  {
    source: "/inspiration",
    destination: "/",
    permanent: false,
  },
];

module.exports = redirectConfig;
