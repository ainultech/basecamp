const withPreact = require("next-plugin-preact");
const nextTranslate = require("next-translate");
const redirectConfig = require("./redirect.config");

const nextConfig = {
  modern: true,
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  async headers() {
    return [
      {
        source: "/:slug*",
        headers: securityHeaders,
      },
    ];
  },
  async headers() {
    return [
      {
        source: "/:slug*",
        headers: [
          // https://community.torproject.org/onion-services/advanced/onion-location/
          {
            key: "Onion-Location",
            value:
              "http://ahmad.ainuldaaaxuecybpo7t4e3he77dxvgj6q27lgx52dgwji23zxpxbgvid.onion/:slug*",
          },
        ],
      },
    ];
  },
  async redirects() {
    return redirectConfig;
  },
  images: {
    domains: [
      "ahmad.ainul.tech",
      "ik.imagekit.io",
      "images.unsplash.com",
      "64.media.tumblr.com",
      "66.media.tumblr.com",
      "pbs.twimg.com",
      "abs.twimg.com",
      "unavatar.io",
      "media2.giphy.com",
      "media0.giphy.com",
      "media3.giphy.com",
      "dl.dropboxusercontent.com",
      "i.imgur.com",
    ],
  },
  reactStrictMode: true,
  future: {
    strictPostcssConfiguration: true,
  },
  experimental: {
    staticPageGenerationTimeout: "360",
    esmExternals: true,
  },
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.(ogg|mp3|wav|mpe?g)$/i,
      exclude: config.exclude,
      use: [
        {
          loader: require.resolve("url-loader"),
          options: {
            limit: config.inlineImageLimit,
            fallback: require.resolve("file-loader"),
            publicPath: `${config.assetPrefix}/_next/static/images/`,
            outputPath: `${isServer ? "../" : ""}static/images/`,
            name: "[name]-[hash].[ext]",
            esModule: config.esModule || false,
          },
        },
      ],
    });

    return config;
  },
};

const config = nextTranslate(nextConfig);
module.exports = withPreact(config);
