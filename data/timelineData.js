export const timelineData2021 = [
  [
    "NFT pertama saya dicetak",
    "Pertama kali saya mencetak foto saya yang sangat penting di OpenSea untuk mengubahnya menjadi NFT.",
    "My first NFT minted",
    "First time I minted my very important photo on OpenSea to turn it into an NFT.",
  ],
  [
    "Berhasil menjaga sahabat saya di samping",
    "Selama 10 bulan, itu waktu yang lama. Saya pikir saya kehilangan teman itu. Sulit untuk terhubung kembali, tetapi saya berhasil. Merasa sangat beruntung.",
    "Successfully keep my best friend beside",
    "For 10 months, that's a long time. I thought I lost that friend. Hard to re-connect, but I did it. Feel so lucky.",
  ],
  [
    "Menjadi Creator di Foundation",
    "Diundang menjadi Kreator, berhak menjual produk NFT di Foundation.app",
    "Becoming a Creator of Foundation",
    "Invited to become a Creator, have the right to sell NFT products on Foundation.app",
  ],
  [
    "Ikuti Program Pelatihan Manajemen Produk Pathwright",
    "Bergabunglah dengan Pathwright dalam 6 bulan untuk belajar dan bekerja di bidang Manajemen Produk",
    "Get into Pathwright Product Management Trainee Program",
    "Join Pathwright in 6 months to learn and work in the field of Product Management",
  ],
  [
    "Timeline Dimulai",
    "Membangun Timeline, mengingat waktu-waktu penting",
    "Started Timeline",
    "Buiding Timeline, remember important milestones",
  ],
];
