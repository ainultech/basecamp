const title = "AINUL.TECH - Ahmad Ainul's personal website";
const description =
  "Halaman pribadi Ahmad Ainul.R. Dibangun dengan Next.js / TailwindCSS dan dihosting di Vercel. Tempat untuk berbagi proyek, blog, dan hal-hal keren lainnya.";

const SEO = {
  title,
  description,
  canonical: "https://ahmad.ainul.tech",
  openGraph: {
    type: "website",
    locale: "id_ID",
    url: "https://ahmad.ainul.tech",
    title,
    description,
    images: [
      {
        url: "https://ahmad.ainul.tech/static/ainul-tech-feature-img.png",
        alt: title,
        width: 1200,
        height: 628,
      },
    ],
  },
};

export default SEO;
