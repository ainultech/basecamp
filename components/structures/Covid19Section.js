import moment from "moment";
import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";
import NextLink from "next/link";

export default function Covid19Section({
  dataGlobal,
  dataIndonesia,
  dataVaccineIndonesia,
}) {
  const router = useRouter();
  const { t } = useTranslation("common");

  const getLastElementOfDataVaccineIndonesia =
    dataVaccineIndonesia[dataVaccineIndonesia.length - 1];

  return (
    <div>
      <div className="inline-flex items-center space-x-1">
        <p className="text-black dark:text-white text-base uppercase font-sourcecodepro">
          /
        </p>
        <h1 className="text-black dark:text-white text-base uppercase font-sourcecodepro">
          Covid19
        </h1>
        <div>
          <NextLink href="/covid19">
            <a className="border-2 border-black dark:border-white text-black dark:text-white text-sm uppercase font-bold px-1 bg-white dark:bg-black hover:bg-black hover:text-white dark:hover:bg-white dark:hover:text-black">
              Open
            </a>
          </NextLink>
        </div>
      </div>
      <div className="px-4 pt-2 space-y-2">
        <div>
          <p className="text-black dark:text-white text-sm">
            {t("covid19-from")}{" "}
            <a
              href="https://disease.sh/"
              target="_blank"
              rel="noreferrer"
              className="font-bold rainbow_text_hover"
            >
              Novel Covid19 API
            </a>
            ,{" "}
            <a
              href="https://github.com/owid/covid-19-data"
              target="_blank"
              rel="noreferrer"
              className="font-bold rainbow_text_hover"
            >
              Our World in Data
            </a>{" "}
            ({t("covid19-noted")})
          </p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="space-y-2 border-2 border-black dark:border-white border-dashed rounded-xl p-3">
            <div className="w-16 h-16 rounded-full">
              <img
                src="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/2wgTJVY171NU2547b8/giphy.gif"
                className="object-cover"
              />
            </div>
            <div className="space-y-1">
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-updated")}:{" "}
                <span className="text-sm font-normal normal-case bg-purple-200 text-black px-1 rounded-full py-[2px]">
                  {moment(Number(dataGlobal.updated))
                    .locale(router.locale === "id-ID" ? "id" : "en")
                    .format("MMMM Do YYYY, h:mm:ss a")}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-population")}:{" "}
                <span className="text-sm font-normal normal-case bg-green-200 text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.population.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-cases")}:{" "}
                <span className="text-sm font-normal normal-case bg-red-200 text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.cases.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataGlobal.todayCases.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-recovered")}:{" "}
                <span className="text-sm font-normal normal-case bg-blue-200 text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.recovered.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataGlobal.todayRecovered.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-critical")}:{" "}
                <span className="text-sm font-normal normal-case bg-yellow-200 text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.critical.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-deaths")}:{" "}
                <span className="text-sm font-normal normal-case bg-black dark:bg-white text-white dark:text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.deaths.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataGlobal.todayDeaths.toLocaleString()}
                </span>
              </p>
              <div className="pt-2 flex items-center space-x-1">
                <p className="text-black dark:text-white text-xs uppercase font-bold">
                  {t("covid19-ratio")}
                </p>
                <div className="pb-1">
                  <p>
                    <span className="text-sm font-normal normal-case bg-red-200 text-black px-1 rounded-full py-[2px]">
                      {dataGlobal.casesPerOneMillion.toLocaleString()}
                    </span>{" "}
                    <span className="text-sm font-normal normal-case bg-black dark:bg-white text-white dark:text-black px-1 rounded-full py-[2px]">
                      {dataGlobal.deathsPerOneMillion.toLocaleString()}
                    </span>{" "}
                  </p>
                </div>
              </div>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-affected-countries")}:{" "}
                <span className="text-sm font-normal normal-case bg-pink-200 text-black px-1 rounded-full py-[2px]">
                  {dataGlobal.affectedCountries.toLocaleString()}
                </span>
              </p>
            </div>
          </div>

          <div className="space-y-2 border-2 border-black dark:border-white border-dashed rounded-xl p-3">
            <div className="w-16 h-16 rounded-full">
              <img
                src="https://ik.imagekit.io/ainul/bridge/https://media.giphy.com/media/WnNsDCy6Gd4ulA7GKh/giphy.gif"
                className="object-cover scale-[1.4]"
              />
            </div>
            <div className="space-y-1">
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-updated")}:{" "}
                <span className="text-sm font-normal normal-case bg-purple-200 text-black px-1 rounded-full py-[2px]">
                  {moment(Number(dataIndonesia.updated))
                    .locale(router.locale === "id-ID" ? "id" : "us")
                    .format("MMMM Do YYYY, h:mm:ss a")}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-population")}:{" "}
                <span className="text-sm font-normal normal-case bg-green-200 text-black px-1 rounded-full py-[2px]">
                  {dataIndonesia.population.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-cases")}:{" "}
                <span className="text-sm font-normal normal-case bg-red-200 text-black px-1 rounded-full py-[2px]">
                  {dataIndonesia.cases.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataIndonesia.todayCases.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-recovered")}:{" "}
                <span className="text-sm font-normal normal-case bg-blue-200 text-black px-1 rounded-full py-[2px]">
                  {dataIndonesia.recovered.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataIndonesia.todayRecovered.toLocaleString()}
                </span>
              </p>
              <p className="text-black dark:text-white text-xs uppercase font-bold">
                {t("covid19-deaths")}:{" "}
                <span className="text-sm font-normal normal-case bg-black dark:bg-white text-white dark:text-black px-1 rounded-full py-[2px]">
                  {dataIndonesia.deaths.toLocaleString()}
                </span>{" "}
                <span className="text-sm font-normal normal-case bg-gray-200 text-black px-1 rounded-full py-[2px]">
                  +{dataIndonesia.todayDeaths.toLocaleString()}
                </span>
              </p>
              <div className="pt-2 flex items-center space-x-1">
                <p className="text-black dark:text-white text-xs uppercase font-bold">
                  {t("covid19-ratio")}
                </p>
                <div className="pb-1">
                  <p>
                    <span className="text-sm font-normal normal-case bg-red-200 text-black px-1 rounded-full py-[2px]">
                      {dataIndonesia.casesPerOneMillion.toLocaleString()}
                    </span>{" "}
                    <span className="text-sm font-normal normal-case bg-black dark:bg-white text-white dark:text-black px-1 rounded-full py-[2px]">
                      {dataIndonesia.deathsPerOneMillion.toLocaleString()}
                    </span>{" "}
                  </p>
                </div>
              </div>
              <div className="space-y-1">
                <p className="text-purple-500 text-xs uppercase font-bold">
                  Vaccine
                </p>
                <div className="space-y-1">
                  <p className="text-black dark:text-white text-xs uppercase font-bold">
                    {t("covid19-updated")}:{" "}
                    <span className="font-normal">
                      {getLastElementOfDataVaccineIndonesia.date}
                    </span>
                  </p>
                  <p className="text-black dark:text-white text-xs uppercase font-bold">
                    {t("covid19-source")}:{" "}
                    <span className="font-normal">
                      <a
                        href={getLastElementOfDataVaccineIndonesia.source_url}
                        target="_blank"
                        rel="noreferrer"
                        className="rainbow_text_hover normal-case"
                      >
                        Kemenkes RI
                      </a>
                    </span>
                  </p>
                  <p className="text-black dark:text-white text-xs uppercase font-bold">
                    {t("covid19-total-vaccinations")}:{" "}
                    <span className="font-normal">
                      {Number(
                        getLastElementOfDataVaccineIndonesia.total_vaccinations
                      ).toLocaleString()}
                    </span>
                  </p>
                  <p className="text-black dark:text-white text-xs uppercase font-bold">
                    {t("covid19-people-vaccinated")}:{" "}
                    <span className="font-normal">
                      {Number(
                        getLastElementOfDataVaccineIndonesia.people_vaccinated
                      ).toLocaleString()}
                    </span>
                  </p>
                  <p className="text-black dark:text-white text-xs uppercase font-bold">
                    {t("covid19-people-fully-vaccinated")}:{" "}
                    <span className="font-normal">
                      {Number(
                        getLastElementOfDataVaccineIndonesia.people_fully_vaccinated
                      ).toLocaleString()}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
