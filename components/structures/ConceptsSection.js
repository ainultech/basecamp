import { Lifesaver } from "akar-icons";
import NextLink from "next/link";

export const conceptsData = [
  [
    "'Maildong - Decentralized Email'",
    "/concepts/maildong-decentralized-email",
    "2021-09-01",
  ],
];
export default function ConceptsSection() {
  return (
    <div>
      <div className="inline-flex items-center space-x-1">
        <p className="text-black dark:text-white uppercase font-sourcecodepro text-base">
          /
        </p>
        <h1 className="text-black dark:text-white uppercase font-sourcecodepro text-base">
          Concepts
        </h1>
      </div>
      <div className="px-4 py-2">
        {conceptsData.reverse().map((b, index) => (
          <div key={index} className="flex items-center space-x-3">
            <div className="pr-2">
              <Lifesaver className="text-black dark:text-white animate-spin-slow-42" />
            </div>
            <div className="flex items-center space-x-2">
              <p className="text-gray-500 dark:text-gray-400 font-bold font-sourcecodepro text-sm inline-flex">
                {b[2]}
              </p>
              <NextLink href={b[1]}>
                <a>
                  <h2 className="text-black dark:text-white inline-block font-bold rainbow_text_hover">
                    {b[0]}
                  </h2>
                </a>
              </NextLink>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
