import useTranslation from "next-translate/useTranslation";
import LetterByAinul from "../elements/LetterByAinul";

export default function HeroSection() {
  const { t } = useTranslation("common");

  return (
    <div>
      <LetterByAinul letterNumber="#0001">
        <div className="text-black dark:text-white text-sm">
          {t("letter-p1a")}{" "}
          <a
            className="font-bold rainbow_text_hover"
            href="https://en.wikipedia.org/wiki/Lamongan_Regency"
            target="_blank"
            rel="noreferrer"
          >
            {t("letter-p1b")}
          </a>
          {t("letter-p1c")}{" "}
          <a
            className="font-bold rainbow_text_hover"
            href="https://postmodernjukebox.com"
            target="_blank"
            rel="noreferrer"
          >
            Postmodern Jukebox
          </a>
          {t("letter-p1d")}
        </div>
      </LetterByAinul>
    </div>
  );
}
